# -*- coding: utf-8 -*-
"""
Created on Fri Jul 28 19:50:42 2017

@author: CPU 2353
"""
import numpy as np
from skimage import io

img = io.imread(r'D:\Texture datasets\MondialMarmi20\00\AcquaMarina_A_00_01.BMP')

def compute_LUT():
    
    num_bits = 8*np.dtype(np.uint8).itemsize
    intensities = np.arange(2**num_bits).astype(np.uint8)[:, None]
    exponents = np.arange(num_bits - 1, -1, -1)
    
    weight_r = np.long(2)**(3*exponents)
    weight_g = np.long(2)**(3*exponents + 1) 
    weight_b = np.long(2)**(3*exponents + 2)

    r = np.sum(weight_r*np.unpackbits(intensities, axis=-1), axis=-1)
    g = np.sum(weight_g*np.unpackbits(intensities, axis=-1), axis=-1)
    b = np.sum(weight_b*np.unpackbits(intensities, axis=-1), axis=-1)

    return r[:, None, None] + g[None, :, None] + b[None, None, :]

lut = compute_LUT()

lut[img[:2, :2, :]]

lut[img[:, :, 0], img[:, :, 1], img[:, :, 2]]
lut[(134, 142, 140)]

lut[137, 147, 142]
#%%



import numpy as np

w3 = np.uint8(2**np.arange(3)[::-1])[:, None]
w8 = 2**np.arange(8)[::-1]


def f_one_pixel(rgb):
    rgb_bits = np.unpackbits(rgb[:, None], axis=1)
    mixed = np.sum(w3*rgb_bits, axis=0)
    return np.dot(mixed, w8)

def f(rgb):
    rgb_bits = np.unpackbits(rgb, axis=-1)
    mixed = np.sum(w3*rgb_bits, axis=0)
    return np.dot(mixed, w8)


rgb = np.asarray([[[1, 2, 4]], [[0, 0, 8]]]).astype(np.uint8)
rgb_bits = np.unpackbits(rgb, axis=-1)
weights = np.long(2)**np.fliplr(np.arange(24).reshape(8, 3).T).ravel()
np.sum(weights*rgb_bits, axis=-1)

def computeLUT(G = 256):
    lut = np.zeros(shape=(G, G, G), dtype=np.long)
    L = np.unpackbits(np.uint8(np.arange(G))).reshape((-1, 8))
    
    for i in xrange(G):
        r = L[i]
        for j in xrange(G):
            g = L[j]
            for k in xrange(G):
                b = L[k]
                lut[i, j, k] = np.dot(np.packbits(np.vstack((r, g, b)).transpose().reshape((1, -1))), [G**2,G,1])
    return lut


def compute_LUT():
    
    num_bits = 8*np.dtype(np.uint8).itemsize
    intensities = np.arange(2**num_bits).astype(np.uint8)[:, None]
    exponents = np.arange(num_bits - 1, -1, -1)
    
    weight_r = np.long(2)**(3*exponents)
    weight_g = np.long(2)**(3*exponents + 1) 
    weight_b = np.long(2)**(3*exponents + 2)

    r = np.sum(weight_r*np.unpackbits(intensities, axis=-1), axis=-1)
    g = np.sum(weight_g*np.unpackbits(intensities, axis=-1), axis=-1)
    b = np.sum(weight_b*np.unpackbits(intensities, axis=-1), axis=-1)

    return r[:, None, None] + g[None, :, None] + b[None, None, :]


lut3 = compute_LUT()
lut2 = computeLUT()





rgb = np.asarray([[[1, 2, 4]]]).astype(np.uint8)
x = np.asarray([1, 2, 4]).astype(np.uint8)
f(x)
rgb = np.array([1, 2, 4]).astype(np.uint8)

perm = (2, 0, 1)
rgb[(perm,)]
np.unpackbits(rgb[(perm,)])

np.packbits(np.unpackbits(rgb[(perm,)][:, None], axis=1).T, axis=1)

a = rgb[(perm,)][:, None]
b = np.unpackbits(a, axis=1)
c = np.packbits(b, axis=0)

w3 = np.uint8(2**np.arange(3)[::-1])
c = b*w3[:, None]
d = np.sum(c, axis=0)
np.dot(d, w8)

#from descriptors_class import Rank_Transform
from skimage import io
#descr = Rank_Transform([1])
#img = io.imread('D:\Texture datasets\PapSmear\Abnormal\ABNORMAL_carcinoma_in_situ_149146635_149146651_001.BMP')
img = io.imread(r'D:\Texture datasets\MondialMarmi20\00\AcquaMarina_A_00_01.BMP')
#X = descr(img)
