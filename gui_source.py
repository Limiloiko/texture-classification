# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Jun 17 2015)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class Main_Frame
###########################################################################

class Main_Frame ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Texture Classification", pos = wx.DefaultPosition, size = wx.Size( 1081,650 ), style = wx.CAPTION|wx.CLOSE_BOX|wx.SYSTEM_MENU|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		self.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_BACKGROUND ) )
		self.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOW ) )
		
		sizer_Main = wx.GridBagSizer( 0, 0 )
		sizer_Main.SetFlexibleDirection( wx.BOTH )
		sizer_Main.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.panel_Data = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		sizer_Data = wx.GridBagSizer( 0, 0 )
		sizer_Data.SetFlexibleDirection( wx.BOTH )
		sizer_Data.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.label_datasets = wx.StaticText( self.panel_Data, wx.ID_ANY, u"Datasets", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.label_datasets.Wrap( -1 )
		self.label_datasets.SetFont( wx.Font( 12, 70, 90, 90, False, wx.EmptyString ) )
		
		sizer_Data.Add( self.label_datasets, wx.GBPosition( 0, 0 ), wx.GBSpan( 1, 1 ), wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.panel_datasets = wx.Panel( self.panel_Data, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.RAISED_BORDER|wx.TAB_TRAVERSAL )
		grid_datasets = wx.BoxSizer( wx.VERTICAL )
		
		self.checkBox_Kather = wx.CheckBox( self.panel_datasets, wx.ID_ANY, u"Kather", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.checkBox_Kather.Enable( False )
		
		grid_datasets.Add( self.checkBox_Kather, 0, wx.ALL, 5 )
		
		self.checkBox_KTHTIPS2b = wx.CheckBox( self.panel_datasets, wx.ID_ANY, u"KTH-TIPs2-b", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.checkBox_KTHTIPS2b.Enable( False )
		
		grid_datasets.Add( self.checkBox_KTHTIPS2b, 0, wx.ALL, 5 )
		
		self.checkBox_KylbergSintorn = wx.CheckBox( self.panel_datasets, wx.ID_ANY, u"Kylberg-Sintorn", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.checkBox_KylbergSintorn.Enable( False )
		
		grid_datasets.Add( self.checkBox_KylbergSintorn, 0, wx.ALL, 5 )
		
		self.checkBox_MondialMarmi = wx.CheckBox( self.panel_datasets, wx.ID_ANY, u"MondialMarmi", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.checkBox_MondialMarmi.Enable( False )
		
		grid_datasets.Add( self.checkBox_MondialMarmi, 0, wx.ALL, 5 )
		
		self.checkBox_Outex_13 = wx.CheckBox( self.panel_datasets, wx.ID_ANY, u"Outex-13", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.checkBox_Outex_13.Enable( False )
		
		grid_datasets.Add( self.checkBox_Outex_13, 0, wx.ALL, 5 )
		
		self.checkBox_PapSmear = wx.CheckBox( self.panel_datasets, wx.ID_ANY, u"PapSmear", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.checkBox_PapSmear.Enable( False )
		
		grid_datasets.Add( self.checkBox_PapSmear, 0, wx.ALL, 5 )
		
		self.checkBox_PlantLeaves = wx.CheckBox( self.panel_datasets, wx.ID_ANY, u"PlantLeaves", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.checkBox_PlantLeaves.Enable( False )
		
		grid_datasets.Add( self.checkBox_PlantLeaves, 0, wx.ALL, 5 )
		
		self.checkBox_RawFooT = wx.CheckBox( self.panel_datasets, wx.ID_ANY, u"RawFooT", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.checkBox_RawFooT.Enable( False )
		
		grid_datasets.Add( self.checkBox_RawFooT, 0, wx.ALL, 5 )
		
		self.checkBox_STex = wx.CheckBox( self.panel_datasets, wx.ID_ANY, u"STex", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.checkBox_STex.Enable( False )
		
		grid_datasets.Add( self.checkBox_STex, 0, wx.ALL, 5 )
		
		
		self.panel_datasets.SetSizer( grid_datasets )
		self.panel_datasets.Layout()
		grid_datasets.Fit( self.panel_datasets )
		sizer_Data.Add( self.panel_datasets, wx.GBPosition( 1, 0 ), wx.GBSpan( 1, 1 ), wx.ALL, 5 )
		
		self.m_staticline1 = wx.StaticLine( self.panel_Data, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_VERTICAL )
		sizer_Data.Add( self.m_staticline1, wx.GBPosition( 0, 2 ), wx.GBSpan( 2, 1 ), wx.EXPAND |wx.ALL, 5 )
		
		self.panel_descriptors = wx.Panel( self.panel_Data, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		grid_descriptors = wx.GridBagSizer( 0, 0 )
		grid_descriptors.SetFlexibleDirection( wx.BOTH )
		grid_descriptors.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.label_descriptors = wx.StaticText( self.panel_descriptors, wx.ID_ANY, u"Descriptor", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.label_descriptors.Wrap( -1 )
		grid_descriptors.Add( self.label_descriptors, wx.GBPosition( 0, 0 ), wx.GBSpan( 1, 1 ), wx.ALIGN_CENTER|wx.ALL, 5 )
		
		combo_descriptorChoices = [ u"Rank", u"Order Lex", u"Order BitMix", u"PreOrder Ref", u"Partial Order" ]
		self.combo_descriptor = wx.ComboBox( self.panel_descriptors, wx.ID_ANY, u"Descriptor", wx.DefaultPosition, wx.DefaultSize, combo_descriptorChoices, 0 )
		grid_descriptors.Add( self.combo_descriptor, wx.GBPosition( 1, 0 ), wx.GBSpan( 1, 1 ), wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.label_bands = wx.StaticText( self.panel_descriptors, wx.ID_ANY, u"Bands", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.label_bands.Wrap( -1 )
		grid_descriptors.Add( self.label_bands, wx.GBPosition( 0, 1 ), wx.GBSpan( 1, 1 ), wx.ALIGN_CENTER|wx.ALL, 5 )
		
		combo_bandsChoices = [ u"RGB", u"RBG", u"GBR", u"GRB", u"BRG", u"BGR" ]
		self.combo_bands = wx.ComboBox( self.panel_descriptors, wx.ID_ANY, u"Combo!", wx.DefaultPosition, wx.DefaultSize, combo_bandsChoices, 0 )
		self.combo_bands.SetSelection( 0 )
		self.combo_bands.Enable( False )
		
		grid_descriptors.Add( self.combo_bands, wx.GBPosition( 1, 1 ), wx.GBSpan( 1, 1 ), wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.label_Reference = wx.StaticText( self.panel_descriptors, wx.ID_ANY, u"Reference", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.label_Reference.Wrap( -1 )
		grid_descriptors.Add( self.label_Reference, wx.GBPosition( 0, 2 ), wx.GBSpan( 1, 1 ), wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.panel_references = wx.Panel( self.panel_descriptors, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.panel_references.Enable( False )
		
		grid_reference = wx.BoxSizer( wx.HORIZONTAL )
		
		self.textCtrl_ref1 = wx.TextCtrl( self.panel_references, wx.ID_ANY, u"0", wx.DefaultPosition, wx.Size( 40,-1 ), 0 )
		grid_reference.Add( self.textCtrl_ref1, 0, wx.ALL, 5 )
		
		self.textCtrl_ref2 = wx.TextCtrl( self.panel_references, wx.ID_ANY, u"0", wx.DefaultPosition, wx.Size( 40,-1 ), 0 )
		grid_reference.Add( self.textCtrl_ref2, 0, wx.ALL, 5 )
		
		self.textCtrl_ref3 = wx.TextCtrl( self.panel_references, wx.ID_ANY, u"0", wx.DefaultPosition, wx.Size( 40,-1 ), 0 )
		grid_reference.Add( self.textCtrl_ref3, 0, wx.ALL, 5 )
		
		
		self.panel_references.SetSizer( grid_reference )
		self.panel_references.Layout()
		grid_reference.Fit( self.panel_references )
		grid_descriptors.Add( self.panel_references, wx.GBPosition( 1, 2 ), wx.GBSpan( 1, 1 ), wx.EXPAND |wx.ALL, 5 )
		
		self.label_scale = wx.StaticText( self.panel_descriptors, wx.ID_ANY, u"Scale", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.label_scale.Wrap( -1 )
		grid_descriptors.Add( self.label_scale, wx.GBPosition( 0, 3 ), wx.GBSpan( 1, 1 ), wx.ALIGN_CENTER|wx.ALL, 5 )
		
		combo_ScaleChoices = [ u"p=[8],r=[1]", u"p=[8,16],r=[1,2]", u"p=[8,16,24],r=[1,2,3]", u"p=[16],r=[2]", u"p=[24],r=[3]", wx.EmptyString ]
		self.combo_Scale = wx.ComboBox( self.panel_descriptors, wx.ID_ANY, u"Scale", wx.DefaultPosition, wx.DefaultSize, combo_ScaleChoices, 0 )
		self.combo_Scale.SetSelection( 0 )
		grid_descriptors.Add( self.combo_Scale, wx.GBPosition( 1, 3 ), wx.GBSpan( 1, 1 ), wx.ALIGN_CENTER|wx.ALL, 5 )
		
		listBox_DescriptorsChoices = []
		self.listBox_Descriptors = wx.ListBox( self.panel_descriptors, wx.ID_ANY, wx.DefaultPosition, wx.Size( -1,75 ), listBox_DescriptorsChoices, 0|wx.HSCROLL|wx.VSCROLL )
		grid_descriptors.Add( self.listBox_Descriptors, wx.GBPosition( 2, 0 ), wx.GBSpan( 1, 3 ), wx.ALL|wx.EXPAND, 5 )
		
		self.button_Add = wx.Button( self.panel_descriptors, wx.ID_ANY, u"Add", wx.DefaultPosition, wx.DefaultSize, 0 )
		grid_descriptors.Add( self.button_Add, wx.GBPosition( 2, 3 ), wx.GBSpan( 1, 1 ), wx.ALL, 5 )
		
		self.button_Delete = wx.Button( self.panel_descriptors, wx.ID_ANY, u"Delete", wx.DefaultPosition, wx.DefaultSize, 0 )
		grid_descriptors.Add( self.button_Delete, wx.GBPosition( 3, 0 ), wx.GBSpan( 1, 1 ), wx.ALIGN_CENTER|wx.ALL, 5 )
		
		
		self.panel_descriptors.SetSizer( grid_descriptors )
		self.panel_descriptors.Layout()
		grid_descriptors.Fit( self.panel_descriptors )
		sizer_Data.Add( self.panel_descriptors, wx.GBPosition( 1, 1 ), wx.GBSpan( 1, 1 ), wx.EXPAND |wx.ALL, 5 )
		
		self.label_validators = wx.StaticText( self.panel_Data, wx.ID_ANY, u"Validators:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.label_validators.Wrap( -1 )
		self.label_validators.SetFont( wx.Font( 12, 70, 90, 90, False, wx.EmptyString ) )
		
		sizer_Data.Add( self.label_validators, wx.GBPosition( 0, 3 ), wx.GBSpan( 1, 1 ), wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.panel_validator = wx.Panel( self.panel_Data, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		grid_validator = wx.GridBagSizer( 0, 0 )
		grid_validator.SetFlexibleDirection( wx.BOTH )
		grid_validator.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.label_validator = wx.StaticText( self.panel_validator, wx.ID_ANY, u"Validator:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.label_validator.Wrap( -1 )
		grid_validator.Add( self.label_validator, wx.GBPosition( 0, 0 ), wx.GBSpan( 1, 1 ), wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT|wx.ALL, 5 )
		
		combo_validatorChoices = [ u"Stratified Sampling Train" ]
		self.combo_validator = wx.ComboBox( self.panel_validator, wx.ID_ANY, u"Validator", wx.DefaultPosition, wx.DefaultSize, combo_validatorChoices, 0 )
		grid_validator.Add( self.combo_validator, wx.GBPosition( 0, 1 ), wx.GBSpan( 1, 1 ), wx.ALL, 5 )
		
		self.label_value = wx.StaticText( self.panel_validator, wx.ID_ANY, u"Split:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.label_value.Wrap( -1 )
		grid_validator.Add( self.label_value, wx.GBPosition( 1, 0 ), wx.GBSpan( 1, 1 ), wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT|wx.ALL, 5 )
		
		self.textCtrl_SSS = wx.TextCtrl( self.panel_validator, wx.ID_ANY, u"0.5", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.textCtrl_SSS.Enable( False )
		
		grid_validator.Add( self.textCtrl_SSS, wx.GBPosition( 1, 1 ), wx.GBSpan( 1, 1 ), wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		listBox_ValidatorsChoices = []
		self.listBox_Validators = wx.ListBox( self.panel_validator, wx.ID_ANY, wx.DefaultPosition, wx.Size( -1,75 ), listBox_ValidatorsChoices, 0 )
		grid_validator.Add( self.listBox_Validators, wx.GBPosition( 2, 0 ), wx.GBSpan( 2, 3 ), wx.ALL|wx.EXPAND, 5 )
		
		self.button_DeleteValidator = wx.Button( self.panel_validator, wx.ID_ANY, u"Delete", wx.DefaultPosition, wx.DefaultSize, 0 )
		grid_validator.Add( self.button_DeleteValidator, wx.GBPosition( 4, 0 ), wx.GBSpan( 1, 1 ), wx.ALL, 5 )
		
		self.button_AddValidator = wx.Button( self.panel_validator, wx.ID_ANY, u"Add", wx.DefaultPosition, wx.DefaultSize, 0 )
		grid_validator.Add( self.button_AddValidator, wx.GBPosition( 2, 4 ), wx.GBSpan( 1, 1 ), wx.ALL, 5 )
		
		
		self.panel_validator.SetSizer( grid_validator )
		self.panel_validator.Layout()
		grid_validator.Fit( self.panel_validator )
		sizer_Data.Add( self.panel_validator, wx.GBPosition( 1, 3 ), wx.GBSpan( 1, 1 ), wx.EXPAND |wx.ALL, 5 )
		
		self.label_Classifiers = wx.StaticText( self.panel_Data, wx.ID_ANY, u"Classifiers:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.label_Classifiers.Wrap( -1 )
		self.label_Classifiers.SetFont( wx.Font( 12, 70, 90, 90, False, wx.EmptyString ) )
		
		sizer_Data.Add( self.label_Classifiers, wx.GBPosition( 2, 3 ), wx.GBSpan( 1, 1 ), wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.panel_classifiers = wx.Panel( self.panel_Data, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		grid_classifiers = wx.GridBagSizer( 0, 0 )
		grid_classifiers.SetFlexibleDirection( wx.BOTH )
		grid_classifiers.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.panel_classtype = wx.Panel( self.panel_classifiers, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer4 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.label_Classifier = wx.StaticText( self.panel_classtype, wx.ID_ANY, u"Classifier:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.label_Classifier.Wrap( -1 )
		bSizer4.Add( self.label_Classifier, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		combo_classifierChoices = [ u"KNN", u"SVM", u"DTC" ]
		self.combo_classifier = wx.ComboBox( self.panel_classtype, wx.ID_ANY, u"Classifier", wx.DefaultPosition, wx.Size( 100,-1 ), combo_classifierChoices, 0 )
		bSizer4.Add( self.combo_classifier, 0, wx.ALL, 5 )
		
		self.check_Best = wx.CheckBox( self.panel_classtype, wx.ID_ANY, u"Best params", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer4.Add( self.check_Best, 0, wx.ALL, 5 )
		
		
		self.panel_classtype.SetSizer( bSizer4 )
		self.panel_classtype.Layout()
		bSizer4.Fit( self.panel_classtype )
		grid_classifiers.Add( self.panel_classtype, wx.GBPosition( 0, 2 ), wx.GBSpan( 1, 1 ), wx.ALL|wx.EXPAND, 5 )
		
		self.panel_classparam = wx.Panel( self.panel_classifiers, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		grid_classparam = wx.GridBagSizer( 0, 0 )
		grid_classparam.SetFlexibleDirection( wx.BOTH )
		grid_classparam.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.label_knnN = wx.StaticText( self.panel_classparam, wx.ID_ANY, u"KNN_N:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.label_knnN.Wrap( -1 )
		grid_classparam.Add( self.label_knnN, wx.GBPosition( 0, 0 ), wx.GBSpan( 1, 1 ), wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT|wx.ALL, 5 )
		
		self.textCtrl_N = wx.TextCtrl( self.panel_classparam, wx.ID_ANY, u"1", wx.DefaultPosition, wx.Size( 50,-1 ), 0 )
		self.textCtrl_N.Enable( False )
		
		grid_classparam.Add( self.textCtrl_N, wx.GBPosition( 0, 1 ), wx.GBSpan( 1, 1 ), wx.ALL, 5 )
		
		self.label_svmC = wx.StaticText( self.panel_classparam, wx.ID_ANY, u"SVM_C:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.label_svmC.Wrap( -1 )
		grid_classparam.Add( self.label_svmC, wx.GBPosition( 1, 0 ), wx.GBSpan( 1, 1 ), wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT|wx.ALL, 5 )
		
		self.textCtrl_C = wx.TextCtrl( self.panel_classparam, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 50,-1 ), 0 )
		self.textCtrl_C.Enable( False )
		
		grid_classparam.Add( self.textCtrl_C, wx.GBPosition( 1, 1 ), wx.GBSpan( 1, 1 ), wx.ALL, 5 )
		
		self.label_svmG = wx.StaticText( self.panel_classparam, wx.ID_ANY, u"SVM_G:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.label_svmG.Wrap( -1 )
		grid_classparam.Add( self.label_svmG, wx.GBPosition( 2, 0 ), wx.GBSpan( 1, 1 ), wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT|wx.ALL, 5 )
		
		self.textCtrl_G = wx.TextCtrl( self.panel_classparam, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 50,-1 ), 0 )
		self.textCtrl_G.Enable( False )
		
		grid_classparam.Add( self.textCtrl_G, wx.GBPosition( 2, 1 ), wx.GBSpan( 1, 1 ), wx.ALL, 5 )
		
		self.lavel_dtcDepth = wx.StaticText( self.panel_classparam, wx.ID_ANY, u"DTC_Depth", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.lavel_dtcDepth.Wrap( -1 )
		grid_classparam.Add( self.lavel_dtcDepth, wx.GBPosition( 0, 2 ), wx.GBSpan( 1, 1 ), wx.ALL, 5 )
		
		self.textCtrl_Depth = wx.TextCtrl( self.panel_classparam, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 50,-1 ), 0 )
		self.textCtrl_Depth.Enable( False )
		
		grid_classparam.Add( self.textCtrl_Depth, wx.GBPosition( 0, 3 ), wx.GBSpan( 1, 1 ), wx.ALL, 5 )
		
		self.label_dtcLeaf = wx.StaticText( self.panel_classparam, wx.ID_ANY, u"DTC_SamplesLeaf", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.label_dtcLeaf.Wrap( -1 )
		grid_classparam.Add( self.label_dtcLeaf, wx.GBPosition( 1, 2 ), wx.GBSpan( 1, 1 ), wx.ALL, 5 )
		
		self.textCtrl_Leaf = wx.TextCtrl( self.panel_classparam, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 50,-1 ), 0 )
		self.textCtrl_Leaf.Enable( False )
		
		grid_classparam.Add( self.textCtrl_Leaf, wx.GBPosition( 1, 3 ), wx.GBSpan( 1, 1 ), wx.ALL, 5 )
		
		
		self.panel_classparam.SetSizer( grid_classparam )
		self.panel_classparam.Layout()
		grid_classparam.Fit( self.panel_classparam )
		grid_classifiers.Add( self.panel_classparam, wx.GBPosition( 1, 1 ), wx.GBSpan( 1, 2 ), wx.EXPAND |wx.ALL, 5 )
		
		listBox_ClassifiersChoices = []
		self.listBox_Classifiers = wx.ListBox( self.panel_classifiers, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, listBox_ClassifiersChoices, 0 )
		grid_classifiers.Add( self.listBox_Classifiers, wx.GBPosition( 2, 1 ), wx.GBSpan( 1, 2 ), wx.ALL|wx.EXPAND, 5 )
		
		self.button_AddClassifier = wx.Button( self.panel_classifiers, wx.ID_ANY, u"Add", wx.DefaultPosition, wx.DefaultSize, 0 )
		grid_classifiers.Add( self.button_AddClassifier, wx.GBPosition( 3, 2 ), wx.GBSpan( 1, 1 ), wx.ALL, 5 )
		
		self.button_DeleteClassifier = wx.Button( self.panel_classifiers, wx.ID_ANY, u"Delete", wx.DefaultPosition, wx.DefaultSize, 0 )
		grid_classifiers.Add( self.button_DeleteClassifier, wx.GBPosition( 3, 1 ), wx.GBSpan( 1, 1 ), wx.ALL, 5 )
		
		
		self.panel_classifiers.SetSizer( grid_classifiers )
		self.panel_classifiers.Layout()
		grid_classifiers.Fit( self.panel_classifiers )
		sizer_Data.Add( self.panel_classifiers, wx.GBPosition( 3, 3 ), wx.GBSpan( 1, 1 ), wx.EXPAND |wx.ALL, 5 )
		
		self.panel_dir = wx.Panel( self.panel_Data, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		grid_dir = wx.BoxSizer( wx.HORIZONTAL )
		
		self.label_DatasetDir = wx.StaticText( self.panel_dir, wx.ID_ANY, u"Datasets directory:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.label_DatasetDir.Wrap( -1 )
		grid_dir.Add( self.label_DatasetDir, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.dirPicker = wx.DirPickerCtrl( self.panel_dir, wx.ID_ANY, wx.EmptyString, u"Select the datasets folder", wx.DefaultPosition, wx.Size( 350,-1 ), wx.DIRP_DEFAULT_STYLE )
		grid_dir.Add( self.dirPicker, 0, wx.ALL, 5 )
		
		
		self.panel_dir.SetSizer( grid_dir )
		self.panel_dir.Layout()
		grid_dir.Fit( self.panel_dir )
		sizer_Data.Add( self.panel_dir, wx.GBPosition( 2, 0 ), wx.GBSpan( 1, 2 ), wx.EXPAND |wx.ALL, 5 )
		
		self.panel_Log = wx.Panel( self.panel_Data, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		Sizer_Log = wx.GridBagSizer( 0, 0 )
		Sizer_Log.SetFlexibleDirection( wx.BOTH )
		Sizer_Log.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		listBox_LogChoices = []
		self.listBox_Log = wx.ListBox( self.panel_Log, wx.ID_ANY, wx.DefaultPosition, wx.Size( 630,200 ), listBox_LogChoices, 0|wx.HSCROLL|wx.VSCROLL )
		Sizer_Log.Add( self.listBox_Log, wx.GBPosition( 0, 0 ), wx.GBSpan( 6, 2 ), wx.ALL|wx.EXPAND, 5 )
		
		
		self.panel_Log.SetSizer( Sizer_Log )
		self.panel_Log.Layout()
		Sizer_Log.Fit( self.panel_Log )
		sizer_Data.Add( self.panel_Log, wx.GBPosition( 3, 0 ), wx.GBSpan( 1, 2 ), wx.ALL|wx.EXPAND, 5 )
		
		
		self.panel_Data.SetSizer( sizer_Data )
		self.panel_Data.Layout()
		sizer_Data.Fit( self.panel_Data )
		sizer_Main.Add( self.panel_Data, wx.GBPosition( 0, 0 ), wx.GBSpan( 1, 1 ), wx.EXPAND |wx.ALL, 10 )
		
		
		self.SetSizer( sizer_Main )
		self.Layout()
		self.menu = wx.MenuBar( 0 )
		self.m_file = wx.Menu()
		self.m_file.AppendSeparator()
		
		self.m_menuexit = wx.MenuItem( self.m_file, wx.ID_ANY, u"Exit", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_file.AppendItem( self.m_menuexit )
		
		self.menu.Append( self.m_file, u"File" ) 
		
		self.m_Project = wx.Menu()
		self.m_Reset = wx.MenuItem( self.m_Project, wx.ID_ANY, u"Reset", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_Project.AppendItem( self.m_Reset )
		
		self.m_Project.AppendSeparator()
		
		self.m_Start = wx.MenuItem( self.m_Project, wx.ID_ANY, u"Start"+ u"\t" + u"F5", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_Project.AppendItem( self.m_Start )
		
		self.m_Stop = wx.MenuItem( self.m_Project, wx.ID_ANY, u"Stop", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_Project.AppendItem( self.m_Stop )
		
		self.menu.Append( self.m_Project, u"Project" ) 
		
		self.m_View = wx.Menu()
		self.m_clear = wx.MenuItem( self.m_View, wx.ID_ANY, u"Clear Log", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_View.AppendItem( self.m_clear )
		
		self.menu.Append( self.m_View, u"View" ) 
		
		self.SetMenuBar( self.menu )
		
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.Bind( wx.EVT_CLOSE, self.m_menuexitOnMenuSelection )
		self.combo_descriptor.Bind( wx.EVT_COMBOBOX, self.combo_descriptorOnCombobox )
		self.button_Add.Bind( wx.EVT_BUTTON, self.button_AddOnButtonClick )
		self.button_Delete.Bind( wx.EVT_BUTTON, self.button_DeleteOnButtonClick )
		self.combo_validator.Bind( wx.EVT_COMBOBOX, self.combo_validatorOnCombobox )
		self.button_DeleteValidator.Bind( wx.EVT_BUTTON, self.button_DeleteValidatorOnButtonClick )
		self.button_AddValidator.Bind( wx.EVT_BUTTON, self.button_AddValidatorOnButtonClick )
		self.combo_classifier.Bind( wx.EVT_COMBOBOX, self.combo_classifierOnCombobox )
		self.check_Best.Bind( wx.EVT_CHECKBOX, self.check_BestOnCheckBox )
		self.button_AddClassifier.Bind( wx.EVT_BUTTON, self.button_AddClassifierOnButtonClick )
		self.button_DeleteClassifier.Bind( wx.EVT_BUTTON, self.button_DeleteClassifierOnButtonClick )
		self.dirPicker.Bind( wx.EVT_DIRPICKER_CHANGED, self.dirPickerOnDirChanged )
		self.Bind( wx.EVT_MENU, self.m_menuexitOnMenuSelection, id = self.m_menuexit.GetId() )
		self.Bind( wx.EVT_MENU, self.m_ResetOnMenuSelection, id = self.m_Reset.GetId() )
		self.Bind( wx.EVT_MENU, self.m_StartOnMenuSelection, id = self.m_Start.GetId() )
		self.Bind( wx.EVT_MENU, self.m_StopOnMenuSelection, id = self.m_Stop.GetId() )
		self.Bind( wx.EVT_MENU, self.m_clearOnMenuSelection, id = self.m_clear.GetId() )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def m_menuexitOnMenuSelection( self, event ):
		event.Skip()
	
	def combo_descriptorOnCombobox( self, event ):
		event.Skip()
	
	def button_AddOnButtonClick( self, event ):
		event.Skip()
	
	def button_DeleteOnButtonClick( self, event ):
		event.Skip()
	
	def combo_validatorOnCombobox( self, event ):
		event.Skip()
	
	def button_DeleteValidatorOnButtonClick( self, event ):
		event.Skip()
	
	def button_AddValidatorOnButtonClick( self, event ):
		event.Skip()
	
	def combo_classifierOnCombobox( self, event ):
		event.Skip()
	
	def check_BestOnCheckBox( self, event ):
		event.Skip()
	
	def button_AddClassifierOnButtonClick( self, event ):
		event.Skip()
	
	def button_DeleteClassifierOnButtonClick( self, event ):
		event.Skip()
	
	def dirPickerOnDirChanged( self, event ):
		event.Skip()
	
	
	def m_ResetOnMenuSelection( self, event ):
		event.Skip()
	
	def m_StartOnMenuSelection( self, event ):
		event.Skip()
	
	def m_StopOnMenuSelection( self, event ):
		event.Skip()
	
	def m_clearOnMenuSelection( self, event ):
		event.Skip()
