# -*- coding: utf-8 -*-
"""
tc.py 

%%%%%%%%%%%%%%%%%%%%%%%%%

        Texture Classification 

%%%%%%%%%%%%%%%%%%%%%%%%%

    Author: Antonio Fernández   --->antfdez@uvigo.es  
            David Lima          --->davidlimastor@hotmail.com
        
    Last change: 19/04/2017 Vigo
    
    Language: Python 2.7
    
%%%%%%%%%%%%%%%%%%%%%%%%%
"""

__all__ = ['data_folder', 'results_folder', 'datasets', 'validators', 
           'classifiers',]

__version__ = '1.0'
__author__ = 'davidlimastor@hotmail.com, antfdez@uvigo.es'

## Module imports
#
# Standard libraries
import os
import sys
sys.path.insert(0, os.path.join(os.getcwd(), 'Resources'))

#import wx
from time import ctime
# Local application 
import sklearn_wrappers as cw
import descriptors_class as dw
import image_datasets as imdata
import utilities as ut


# Folders DESPACHO
data_folder = r'D:\Texture datasets'

# Folders PORTATIL
#data_folder = r'C:\Users\CPU 2353\OneDrive\STUFF-OneDrive\David'

# Folders UNIVERSIDAD:
#data_folder = r'D:\Programa\Texture datasets'

results_folder = os.path.join(data_folder, '_Results')

datasets = [
#    imdata.Kather(os.path.join(data_folder, 'Kather')),
#    imdata.KTHTIPS2b(os.path.join(data_folder, 'KTH-TIPS2-b')),    
#    imdata.KylbergSintorn(os.path.join(data_folder, 'Kylberg-Sintorn')),
#    imdata.MondialMarmi20(os.path.join(data_folder, 'MondialMarmi20')),
#    imdata.Outex_13(os.path.join(data_folder, 'Outex-13')),
    imdata.PapSmear(os.path.join(data_folder, 'PapSmear')),
#    imdata.PlantLeaves(os.path.join(data_folder, 'PlantLeaves')),
#    imdata.RawFooT(os.path.join(data_folder, 'RawFooT')),
#    imdata.STex(os.path.join(data_folder, 'STex'))
    ]
    
descriptors = [
     dw.Rank_Transform([1]),

#     dw.Rank_Color_Order_Lexicographic('RGB',      [8],    [1]),
#     
#     dw.Rank_Color_Order_Lexicographic('BGR',      [8],    [1]),
#
#     dw.Rank_Color_Order_Lexicographic('GBR',      [8],    [1]),
#
#     dw.Rank_Color_Order_BitMixing('RGB',      [8],     [1]),
#
#     dw.Rank_Color_Order_BitMixing('BGR',      [8],     [1]),
#
#     dw.Rank_Color_Order_BitMixing('GBR',      [8],     [1]),
#
#     dw.Rank_Color_PreOrder_ColorRef(      [8],     [1], [0,0,0]),
#
#     dw.Rank_Color_PreOrder_ColorRef(      [8],     [1], [127,127,127]),
#
#     dw.Rank_Color_PartialOrder(      [8],     [1]),
#     dw.Rank_Color_PartialOrder(   [8,16],   [1,2]),
#     dw.Rank_Color_PartialOrder([8,16,24], [1,2,3]),
    ]

validators = [  
#    cw.SSS(train_size=0.25),
    cw.SSS(train_size=0.5),
#    cw.SSS(train_size=0.75),
    ] 

classifiers = [
    cw.KNN(n_neighbors=1, best=True),
#    cw.SVM(best=True),
#    cw.DTC(best=True),
#    cw.NB(),
#    RandomForestClassifier(),
#    MLPClassifier()
    ]
    
             
def main():
    
    # Name and path of the log.txt file
    folder = os.path.join(data_folder, '_Log')
    ut.check_if_exists(folder)
    
    log_file = os.path.join(folder, ctime().replace(':', '-') + 'SCRIPT.txt')



    
    # Open the file with write mode
    log = open(log_file, 'w')
    
    
    # Read images from disk and compute features (if necessary) 
    for dataset in datasets:
        
        print dataset.name
        
        for descriptor in descriptors:
            print descriptor.__str__()
            
            feats_fname = ut.features_fname(dataset, descriptor, results_folder)
            
            if not os.path.isfile(feats_fname):
                X = ut.compute_features(dataset, descriptor, print_info='True')
                ut.save_to_disk(X, feats_fname)
     
    print '\n\n\n'
    
    
    
    # Read features from disk, split into train/test, classify and compute success rate
    for dataset in datasets:
        line = '###################\n' + dataset.name + '\n'
        print line
        log.write(line)
        
        for descriptor in descriptors:
            line = '--------------------\n' + descriptor.__str__() + '\n'
            print line
            log.write(line)
            feats_fname = ut.features_fname(dataset, descriptor, results_folder)
            
            for validator in validators:
                line = 'Validator: ' + validator.__str__() + '\n'
                print line
                log.write(line)
                
                for classifier in classifiers:
                    
                    
                    accs_fname = ut.accuracies_fname(dataset, 
                                                  descriptor, 
                                                  validator,
                                                  classifier,
                                                  results_folder)
                    X = ut.load_from_disk(feats_fname)
                    
                    
                    y = dataset.labels
                        
                    success_rates = ut.compute_or_read_accuracies(X, y, 
                                                               accs_fname, 
                                                               dataset, 
                                                               descriptor,
                                                               validator, 
                                                               classifier, 
                                                               results_folder, 
                                                               log)
                    
                    line = '\t\tAverage success rate =\t\t%g %%\n' % (success_rates.mean()*100,)
                    print line
                    log.write(line)
                    
    log.close()



if __name__ == '__main__':    
    
    main()


