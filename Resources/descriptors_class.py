# -*- coding: utf-8 -*-
"""descriptors_class.py 

%%%%%%%%%%%%%%%%%%%%%%%

Texture Classification Descriptors

%%%%%%%%%%%%%%%%%%%%%%%

    Author: David Lima         --->  davidlimastor@hotmail.com
            Antonio Fernández  --->  antfdez@uvigo.es  
            
        
    Last change: 29/07/2017 Vigo
    
    Language: Python 2.7

            
    References:
        
    R. Zabih and J. Woodfill. Non-parametric local transforms for computing 
    visual correspondence. In Proceedings of the 3rd European Conference on 
    Computer Vision (ECCV 1994), pages 151–158, Stockholm, Sweden, May 1994. 
    Springer-Verlag.
    
    A. Ledoux, O. Losson, and L. Macaire. Color local binary patterns: 
    Compact descriptors for texture classification. 
    Journal of Electronic Imaging, 25(6):061404–1–12, 2016.
    
"""


__all__ = []
__version__ = '1.0'
__author__ = ['davidlimastor@hotmail.com', 'antfdez@uvigo.es']


import numpy as np
from skimage import color
import utilities as ut


class Descriptor(object):
    """Superclass for descriptors."""       
    
    def __init__(self, radius=[1]):
        """Constructor of Descriptor class. 
        
        Parameters:
        radius (list of int) -- Radii of the local neighborhoods.
        """
        self.radius = radius
        self.points = [8*r for r in radius]
        
        
    def histogram(self, codes, dim):   
        """Compute the histogram of pattern codes (feature values).
        
        Parameters:
        codes (np.array of int) -- Array of feature values.
        dim (int) -- Number of bins of the computed histogram, i.e. the 
                     number of possible feature values.                
        Returns:
        hist (np.array) -- Normalized histogram.  
        """   
        try:
            hist, _ = np.histogram(codes.ravel(),
                                   bins=np.arange(dim),
                                   range=(0, dim))            
            hist = hist.astype('float')
            hist /= hist.sum()            
            return hist        
        except:
            print 'Exception on %s.histogram() method' % (self.__class__,)
            raise

      
    def __call__(self, img):
        """Compute the feature vector (histogram of feature values).
           
        Parameters:
        img (np.array) -- Input image.                
        Returns:
        h (np.array) -- Feature vector, i.e. histogram of the 
                        computed feature values.
        """
        try:
            # Remove the transparency layer if necessary
            if img.ndim == 3 and img.shape[2] == 4:
                img = img[:, :, :3]            
            histogram = []            
            for radius, hist_dim in zip(self.radius, self.hist_dim):                
                codes = self.codemap(img, radius)            
                hist = self.histogram(codes, hist_dim + 1)
                histogram.append(hist)
            h = np.concatenate(histogram, 0)
            return h            
        except:
            print 'Exception on %s.__call__() method' % (self.__class__,)
            raise 

                
    def __str__(self):
        """"Return a string representation of the descriptor."""
        raise NotImplementedError("Subclasses should implement this!")

        
    def codemap(self):
        """Return a map of feature values."""
        raise NotImplementedError("Subclasses should implement this!")

        
        
class Rank_Transform(Descriptor):
    """Return the Rank Transform."""
    

    def __init__(self, radius=[1]):
        """Constructor of the Rank_Transform class.
        
        Parameters:
        radius (list of int) -- Radii of the local neighborhoods.
        """        
        super(Rank_Transform, self).__init__(radius)
        self.name = 'Rank_Transform'            
        # Dimensions of each histogram:
        self.hist_dim = [points + 1 for points in self.points]        
        self.dim = sum(self.hist_dim)        
        self.label = 'Rank Transform'

        
    def __call__(self, img):
        """Compute the Rank Transform of the given image.
            
        Parameters:
        img (np.array) -- Input image.
        Returns:
        h (np.array) -- Normalized histogram of the computed features.
        """
        # Convert to grayscale if necessary
        if img.ndim > 2:
            img = color.rgb2gray(img)
        h = super(Rank_Transform, self).__call__(img)
        return h

            
    def codemap(self, img, radius):
        """Compute the Rank Transform features values.
         
        Parameters:
        img (numpy.ndarray) -- Single channel image.
        radius (int) -- Radius of the local neighborhood.
        Returns:
        codes (np.array) -- Map of feature values.
        """  
        try:
            points = 8*radius
            central = img[radius:-radius, radius:-radius]
            codes = np.zeros_like(central)            
            for pixel in range(points):
                codes += central > ut.window(img, pixel, radius, color=False)
            return codes    
        except:
            print 'Exception on %s.codemap() method' % (self.__class__,)
            raise
            
        
    def __str__(self):
        return 'RT_radius=%s' % (self.radius,)
        
    

class Rank_Transform_Product_Order(Descriptor):
    """Return the Rank Transform based on Product Order."""
    
    
    def __init__(self, radius=[1]):
        """Constructor of the Rank_Transform_Product_Order class.
        
        Parameters:
        radius (list of int) -- Radii of the local neighborhoods.
        """                
        super(Rank_Transform_Product_Order, self).__init__(radius)        
        self.name = 'Rank_Transform_Product_Order'        
        # Dimensions of each histogram:
        self.hist_dim = [(points + 2)*(points + 1)/2 for points in self.points]        
        self.dim = sum(self.hist_dim)                
        self.label = 'Rank Transform Partial Order'


    def codemap(self, img, radius):
        """Compute Rank Transform based on Product Order feature values. 
         
        Parameters:
        img (numpy.ndarray) -- Color image.
        radius (int) -- Radius of the local neighborhood.
        Returns:
        codes (np.array) -- Map of feature values.
        """
        try:
            points = 8*radius
            lt = np.ones(shape=(img.shape[0] - 2*radius, 
                                img.shape[1] - 2*radius,
                                points), dtype='bool')
            ge = np.ones(shape=(img.shape[0] - 2*radius, 
                                img.shape[1] - 2*radius, 
                                points), dtype='bool')    
            central = img[radius: -radius, radius: -radius, :]    
            for pixel in range(points):
                lt[:, :, pixel] = np.all(
                        ut.window(img, pixel, radius) < central, 
                        axis=-1)                    
                ge[:, :, pixel] = np.all(
                        ut.window(img, pixel, radius) >=central, 
                        axis=-1)                    
            num_lt = lt.sum(-1)        
            num_nc = points - num_lt - ge.sum(-1)           
            codes = num_nc + num_lt*(2*points + 3 - num_lt)/2
            return codes        
        except:
            print 'Exception on %s.codemap() method' % (self.__class__,)
            raise
        

    def __str__(self):
        return 'RTPO_radius=%s' % (self.radius,)


        
class Rank_Transform_Lexicographic(Descriptor):
    """Return the Rank Transform based on Lexicographic Order."""
    
    
    def __init__(self, bands='RGB', radius=[1]):
        """Constructor of the Rank_Transform_Lexicographic class.
        
        Parameters:
        bands (str) -- Band priorities.
        radius (list of int) -- Radii of neighborhoods.
        """
        super(Rank_Transform_Lexicographic, self).__init__(radius)
        self.name = 'Rank_Transform_Lexicographic'
        # Dimensions of each histogram:
        self.hist_dim = [points + 1 for points in self.points]
        self.dim = sum(self.hist_dim)
        bands = ['R', 'G', 'B']
        self.bands = bands
        self.perm = tuple(['RGB'.index(band) for band in bands])
        self.label = 'Lex {}'.format(bands)

        
    def codemap(self, img, radius):
        """Compute Rank Transform based on Lexicographic Order features. 
         
        Parameters:
        img (numpy.ndarray) -- Color image.
        radius (int) -- Radius of the local neighborhood.
        Returns:
        codes (np.array) -- Map of feature values.
        """
        try:
            points = 8*radius
            central = img[radius: -radius, radius: -radius, :]
            codes = np.zeros(shape=(img.shape[0] - 2*radius, 
                                    img.shape[1] - 2*radius))
            first, second, third = self.perm
            for pixel in range(points):
                w = ut.window(img, pixel, radius)
                one = w[:, :, first] < central[:, :, first]
                two = (w[:, :, first] == central[:, :, first]) & \
                      (w[:, :, second] < central[:, :, second])
                three = (w[:, :, first] == central[:, :, first]) & \
                        (w[:, :, second] == central[:, :, second]) & \
                        (w[:, :, third] <= central[:, :, third])
                codes += (one | two | three)            
            return codes
        except:
            print 'Exception on %s.codemap() method' % (self.__class__,)
            raise
    

    def __str__(self):
        return 'RTLex_%s_radius=%s' % (self.bands, self.radius)
        
        
        
class Rank_Transform_Bit_Mixing(Descriptor):
    """Return the Rank Transform based on Bix Mixing Order."""
    
    
    def __init__(self, bands='RGB', radius=[1]):
        """Constructor of the Rank_Transform_Bit_Mixing class.
        
        Parameters:
        bands (str) -- Band priorities.
        radius (list of int) -- Radii of neighborhoods.
        """
        super(Rank_Transform_Bit_Mixing, self).__init__(radius)
        self.name = 'Rank_Transform_BitMixing'        
        self.bands = bands
        # Dimensions of each histogram:
        self.hist_dim = [points + 1 for points in self.points]            
        self.dim = sum(self.hist_dim)                    
        self.perm = tuple(['RGB'.index(band) for band in bands])
        self.label = 'RTBitMix {}'.format(bands)
    
    
    def codemap(self, img, radius):
        """Compute Rank Transform based on Bit Mixing feature values. 
         
        Parameters:
        img (numpy.ndarray) -- Color image.
        radius (int) -- Radius of the local neighborhood.
        Returns:
        codes (np.array) -- Map of feature values.
        """
        try:
            points = 8*radius
            lut = self.compute_LUT()            
            central = img[radius: -radius, radius: -radius, self.perm]
            central_order = lut[central[:, :, 0], 
                                central[:, :, 1], 
                                central[:, :, 2]]
            codes = np.zeros(shape=(img.shape[0] - 2*radius, 
                                    img.shape[1] - 2*radius))                
            for pixel in range(points):
                w = ut.window(img, pixel, radius)[:, :, self.perm]
                w_order = lut[w[:, :, 0], w[:, :, 1], w[:, :, 2]]
                codes +=  w_order < central_order            
            return codes
        except:
            print 'Exception on %s.codemap() method' % (self.__class__,)
            raise


    def compute_LUT(self):
        """Return the lookup table for computing Bix Mixing Order."""        
        num_bits = 8*np.dtype(np.uint8).itemsize
        intensities = np.arange(2**num_bits).astype(np.uint8)[:, None]
        exponents = np.arange(num_bits - 1, -1, -1)        
        weight_r = np.long(2)**(3*exponents)
        weight_g = np.long(2)**(3*exponents + 1) 
        weight_b = np.long(2)**(3*exponents + 2)    
        r = np.sum(weight_r*np.unpackbits(intensities, axis=-1), axis=-1)
        g = np.sum(weight_g*np.unpackbits(intensities, axis=-1), axis=-1)
        b = np.sum(weight_b*np.unpackbits(intensities, axis=-1), axis=-1)    
        return r[:, None, None] + g[None, :, None] + b[None, None, :]
            
        
    def __str__(self):
        return 'RTMix_%s_radius=%s' % (self.bands, self.radius)
    
        
class Rank_Transform_Reference_Color(Descriptor):
    """Return the Rank Transform based on a Reference Color."""

    
    def __init__(self, radius=[1], cref=[0, 0, 0]):
        """Constructor of the Rank_Transform_Reference_Color class.
        
        Parameters:
        radius (list of int) -- Radii of neighborhoods.
        cref (list of int) -- RGB coordinates of the reference color.
        """        
        super(Rank_Transform_Reference_Color, self).__init__(radius)
        self.name = 'Rank_Transform_Reference_Color'
        self.cref = cref        
        # Dimensions of each histogram:
        self.hist_dim = [points + 1 for points in self.points]                        
        self.dim = sum(self.hist_dim)         
        self.label = 'RTColRef {}'.format(cref[0])
        
        
    def codemap(self, img, radius):
        """Compute Rank Transform based on a Reference Color feature values. 
         
        Parameters:
        img (numpy.ndarray) -- Color image.
        radius (int) -- Radius of the local neighborhood.
        Returns:
        codes (np.array) -- Map of feature values.
        """
        try:
            points = 8*radius
            cref = np.asarray(self.cref).astype(np.int)
            central = img[radius: -radius, radius: -radius, :]
            codes = np.zeros(shape=(img.shape[0] - 2*radius, 
                                    img.shape[1] - 2*radius))                
            dist_central = np.linalg.norm(central - cref)
            for pixel in range(points):
                w = ut.window(img, pixel, radius)
                dist_w = np.linalg.norm(w - cref)
                codes += dist_w < dist_central
            return codes
        except:
            print 'Exception on %s.codemap() method' % (self.__class__,)
            raise
            
            
    def __str__(self):
        return 'RTRef_color=%s_radius=%s' % (self.cref, self.radius)