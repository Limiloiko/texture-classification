# -*- coding: utf-8 -*-
"""sklearn_wrappers.py 

%%%%%%%%%%%%%%%%%%%%%%%

Texture Classification classifiers

%%%%%%%%%%%%%%%%%%%%%%%

    Author: David Lima          ---> davidlimastor@hotmail.com
            Antonio Fernández   ---> antfdez@uvigo.es  
            
        
    Last change: 02/07/2017 Vigo
    
    Language: Python 2.7
    
"""


__all__ = []
__version__ = '1.0'
__author__ = ['davidlimastor@hotmail.com','antfdez@uvigo.es']


# Module imports
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier


class SSS(StratifiedShuffleSplit):
    def __init__(self, n_splits=100, train_size=0.5, random_state=0):
        """Constructor of the SSS validator.
        
        Args:
            n_splits (int): Number of splits of the data.
            train_size (float): % of data division.
            random_state (int): seed for randoms operations.
        """
        super(SSS, self).__init__(n_splits, 1- train_size, None, random_state)
        
        
    def __str__(self):
        """Print info of the validator.
            
        Example: stratified_sampling_train=0.5
        
        """
        return 'stratified_sampling_train=%g' % (1 - self.test_size,)


        
class KNN(KNeighborsClassifier):
    def __init__(self, n_neighbors=1, best=False):
        """Constructor of the KNN classifier.
        
        Args:
            n_neighbors (int): Value of K nearest neighbors.
            best (bool):If wants to compute the best params for the classifier.
        """
        super(KNN, self).__init__(n_neighbors, 'uniform', 
                                  'brute', 30, 2, 'minkowski', None, 1)
        self.label = 'KNN'
        self.best = best
        
        
    def __str__(self):
        """Print info of the class.
        
        Example: 1NN 
        """
        return '%dNN' % (self.n_neighbors,)



class SVM(SVC):
    def __init__(self, C=1.0, gamma='auto',best=False):
        """Constructor of the SVM classifier.
        
        Args:
            C (float): Penalty parameter C of the error term.
            gamma (float): Kernel coefficient.
        """
        super(SVM, self).__init__(C=C, kernel='rbf', degree=3,
                                 gamma=gamma, coef0=0.0, 
                                 shrinking=True, probability=False, 
                                 tol=1e-3, cache_size=200, 
                                 class_weight=None, verbose=False, 
                                 max_iter=-1, decision_function_shape=None, 
                                 random_state=None)
        self.c = C
        self.gam = gamma
        self.label = 'SVM'
        self.best = best
        
        
    def __str__(self):
        """Print info for SVC class.
        
        Example: SVM C=32, gamma=512
        
        """
        return 'SVM C=%s , gamma=%s' % (self.c, self.gam)



class DTC(DecisionTreeClassifier):
    def __init__(self, max_depth=3, min_samples_leaf=5,
                 min_samples_split=2, best=False):
        """Constructor of the DTC classifier.
        
        Args:
            max_depth (int): Maximum depth of the tree.
            min_samples_leaf(int): Mininum samples per leaf node.
            min_samples_split(int): Minimum samples required to split.
        """
        
        super(DTC, self).__init__(criterion='gini', splitter='best', 
                                 max_depth=max_depth,
                                 min_samples_split=min_samples_split, 
                                 min_samples_leaf=min_samples_leaf, 
                                 min_weight_fraction_leaf=0.0, 
                                 max_features=None, random_state=None, 
                                 max_leaf_nodes=None, min_impurity_split=1e-07, 
                                 class_weight=None, presort=False)
        self.deph = max_depth
        self.min_samples_leaf = min_samples_leaf
        self.label = 'DTC'
        self.best = best
     
        
    def __str__(self):
        """Print info for DTC class.
        
        Example: DTC Depth=13, min_samp_leaf-split=5-3
        
        """
        return 'DTC Depth=%s, min_samp_leaf-split=%s-%s' % (self.deph, 
                                                       self.min_samples_leaf,
                                                       self.min_samples_split)