# -*- coding: utf-8 -*-
"""image_dataset.py 

%%%%%%%%%%%%%%%%%%%%%%%

Texture Classification datasets

%%%%%%%%%%%%%%%%%%%%%%%

    Author: David Lima          ---> davidlimastor@hotmail.com
            Antonio Fernández   ---> antfdez@uvigo.es  
            
        
    Last change: 02/07/2017 Vigo
    
    Language: Python 2.7
    
"""


__all__ = ['datasets_info_folder', 'image_extensions']
__version__ = '1.0'
__author__ = ['davidlimastor@hotmail.com','antfdez@uvigo.es']


# Standard libraries
import os
import numpy as np


datasets_info_folder = os.path.join(os.getcwd(), 'Info', 'Datasets')

## Global variables:
ext_lower = ('.jpg', '.bmp', '.tif', '.png', '.pnm')

# Generator to add upp extension, (maybe windows 10 have this)
image_extensions = ext_lower + tuple(ext.upper() for ext in ext_lower)


## Implementation of general Image Dataset classs
class ImageDataset(object):
    """Implementation of general Image Dataset classs."""
        
                
    def __init__(self, imgdir, overwrite='False'):
        """Constructor of the class.
        
        Args:
            
            imgdir (io.path ):Path where the images are found.
        """
        
        self.name = os.path.split(imgdir)[-1]
        self.images = self.recursive_glob(imgdir)
        self.classes = {text: num for num, text in enumerate(sorted(
                        set(self.get_class(img) for img in self.images)))}
        
        self.labels = np.asarray(
            [self.get_label(img) for img in self.images])
        
        
    def recursive_glob(self, folder='.', extensions=image_extensions):
        """Match the full path names of all the files in folder (and its 
        subfolders) with any of the specified image extensions.
            
        Note: glob is case-insensitive, i.e. for '\*.jpg' you will getfiles 
        ending with .jpg and .JPG.               
        """
        
        return [os.path.join(root, filename)
                for root, dirs, files in os.walk(folder)
                for ext in extensions
                for filename in files if filename.endswith(ext)]


    ## Returns the label associated with the class that the image is from
    def get_label(self, img):
        """Returns the label associated with the class that the image is from.
        """
        
        return self.classes[self.get_class(img)]
    

    def get_class(self, img):
        raise NotImplementedError("Subclasses should implement this!")            
    
    
    def get_rotation(self, img):
        raise NotImplementedError("Subclasses should implement this!")
       
        
    def get_scale(self, img):
        raise NotImplementedError("Subclasses should implement this!")
        
        
    def get_point_of_view(self, img):
        raise NotImplementedError("Subclasses should implement this!")

        

## Implementation of MondialMarmi20 dataset of images
class MondialMarmi20(ImageDataset):
    """Implementation of MondialMarmi20 dataset of images.

    Images info:
            Comprises 25 classes of marble and granite products identified 
            by their commercial denominations, e.g. Azul Platino, Bianco Sardo,
            Rosa Porriño and Verde Bahía. Each class is represented by four 
            tiles; ten images for each tile were acquired under steady 
            illumination conditions and at rotation angles from 0º deg to 90º 
            by steps of 10º. Each class have 16 number of samples.
            
            Extension  :.bmp
            Resolution : 1500x1500
            shape      : (1500,1500,3) RGB
            
            An example of img name : AcquaMarina_A_00_02.bmp
                                        class   : AquaMarina_A
                                        rotation: 00
                                        sample  : 02
                                        extension: .bmp
    """
    
    def __init__(self, imgdir):
                
        super(MondialMarmi20, self).__init__(imgdir)
        
        self.rotations = np.asarray(
                [self.get_rotation(img) for img in self.images])
          
        self.label = 'MondialMarmi'
        
        
    ## Returns the class of the given image
    def get_class(self, img):
        """Returns the class of the given image."""
        
        head, tail = os.path.split(img)
        return tail[:-10]

                            
    ## Returns the value of rotation of the given image
    def get_rotation(self, img):
        """Returns the value of rotation of the given image"""
        
        head, tail = os.path.split(img)
        return int(tail[-9:-7])
    
    

## Implementation of the KTHTIPS2b dataset of images
class KTHTIPS2b(ImageDataset):
    """Implementation of the KTHTIPS2b dataset of images.
        
    Images info:
            Comprises 11 classes of materials (432 sample images per class) 
            and is actually an extension of KTH-TIPS. The image acquisition 
            settings were the same as in KTH-TIPS, Each class have 16 number 
            of samples.

            Extension  :.png
            Resolution : 200x200 Variable
            shape      : (-1,-1,3) RGB
            
            Img example : aluminium_foil\sample_d\15d-scale_2_im_1_col.png
                                        class           : aluminium_foil
                                        point_of_view   : Frontal
                                        scale           : 2
                                        illumination    : Frontal
    """
    
    def __init__(self, imgdir):
        
        super(KTHTIPS2b, self).__init__(imgdir)
        
        self.scales = np.asarray(
                    [self.get_scale(img) for img in self.images])
        self.points_of_view = np.asarray(
                    [self.get_point_of_view(img) for img in self.images])
        self.illumination = np.asarray(
                    [self.get_illumination(img) for img in self.images])
        
        self.label = 'KTH-TIPS2b'
            
             
    ## Returns the class of the given img
    def get_class(self, img):
        """Returns the class of the given img."""
            
        dir = os.path.dirname(img).split('\\')
        return dir[-2]

                            
    ## Returns the scale of the given img
    def get_scale(self, img):
        """Returns the scale of the given img."""
        
        head, tail = os.path.split(img)
        index = tail.find('scale')
        scale = tail[index + 6:index + 8].replace('_', '')
        return scale
    
        
    ## Returns the point_of_view of the given img
    def get_point_of_view(self, img):
        """Returns the point_of_view of the given img."""
        
        head, tail = os.path.split(img)
        indexPoint = tail.find('im')
        im_str = tail[indexPoint+2:indexPoint+5]
        im_str = im_str.replace('_', '')
        
        if im_str in ['1','2','3','10']:
            return 'Frontal'
        elif im_str in ['4','5','6','11']:
            return '22.5 Right'
        elif im_str in ['7','8','9','12']:
            return '22.5 Left'
        else:
            raise ValueError('Incorrect image name')

            
    ## Returns the illumination of the given img
    def get_illumination(self, img):
        """Returns the illumination of the given img."""
        
        head, tail = os.path.split(img)
        indexPoint = tail.find('im')
        im_str = tail[indexPoint+2:indexPoint+5]
        im_str = im_str.replace('_', '')
         
        if im_str in ['1','4','7']:
            return 'Frontal'
        elif im_str in ['2','5','8']:
            return '45 top'
        elif im_str in ['3','6','9']:
            return '45 side'
        elif im_str in ['10','11','12']:
            return 'Ambient'
        else:
            raise ValueError('Incorrect image name') 


           
## Implementation of STex dataset of images
class STex(ImageDataset):
    """Implementation of STex dataset of images.
    
    
    Images info:
            The Salzburg Texture Image Database (STex) is a large collection 
            of 476 color texture image that have been captured around Salzburg,
            Austria. It comprises 32 different classes

            Extension  :.pnm
            Resolution : 1024x1024
            shape      : (1024,1024,3) RGB
            
            An example of img name : Flower.0008.pnm
                                        class           : Flower
                                        Sample          : 0008
    """
    
    def __init__(self, imgdir):
        
        super(STex, self).__init__(imgdir)
        self.label = 'STex'
            
            
    ## Returns the class of the given img
    def get_class(self, img):
        """Returns the class of the given img."""
        
        head, tail = os.path.split(img)
        return tail[:-7]
    
    

## Implementation of Kylberg Sintorn dataset of images
class KylbergSintorn(ImageDataset):
    """Implementation of Kylberg Sintorn dataset of images.
                
    Images info:
            Is composed of 25 classes of heterogeneous materials, such as 
            food (e.g. lentils, oatmeal and sugar), fabric (e.g. knitwear and 
            towels). For each class one sample image was acquired using 
            invariable illumination conditions and under nine different 
            rotation angles. 

            Extension  :.png
            Resolution : 5184 x 3456
            shape      : (3456, 5184, 4) RGB + Transparency
            
            An example of img name : canesugar01-r000.png
                                        class           : canesugar01
                                        Sample          : 01
                                        rotation        : 000 grades
                                        
    Remarks:
        -In this dataset we need to crop images.
    """
     
    def __init__(self, imgdir):
        
        super(KylbergSintorn, self).__init__(imgdir)        
        self.labels = []                
        
        for img in self.images:
            
            for i in range(36):
                self.labels.append(self.get_label(img))
                
        self.labels = np.asarray(self.labels)
        
        self.rotations = np.asarray(
                [self.get_rotation(img) for img in self.images])
        
        self.label = 'Kylberg-Sintorn'    
        
            
    ## Returns the class of the given img
    def get_class(self, img):
        """Returns the class of the given img."""
        
        head, tail = os.path.split(img)
        return tail[:-9]


    ## Returns the img rotation 
    def get_rotation(self, img):
        """Returns the img rotation."""
        
        head, tail = os.path.split(img)
        return int(tail[-7:-4])
    
    
    
## Implementation of the Outex_13 dataset 
class Outex_13(ImageDataset):
    """Implementation of the Outex_13 dataset.
         
    Images info:
            Is composed of 68 classes, acquired under invariable illumination
            conditions.

            Extension  :.bmp
            Resolution : 746 x 538
            shape      : (538, 746, 3) RGB
            
            An example of img name : tile003-inca-100dpi-00.bmp
                                        class           : tile003
                                        
    Remarks: 
        -Visit OUTEX web site for more information abaut this test suit (13)
        
     
    """
     
    def __init__(self, imgdir):
         
        super(Outex_13, self).__init__(imgdir)
        
        
        self.label = 'Outex-13'
        
        
    ## Returns the class of the given img
    #
    def get_class(self, img):
         """
             Returns the class of the given img
         
         """
         
         head, tail = os.path.split(img)
         head = head.split('\\')
         return head[-1]     



## Implementation of the RawFooT dataset
#           
class RawFooT(ImageDataset):
    """
        Implementation of the RawFooT dataset
                
    Images info:
            Comprehends 68 classes(46 samples per class) of raw food and grains
            such as corn, chicken breast, pomegranate, salmon and tuna. 
            The materials were acquired under 46 different illumination 
            conditions resulting in as many image samples for each class. 

            Extension  : .png
            Resolution : 800x800
            shape      : 800x800x3
                
            An example of img name :  0007-01
                                        class : 007
                                        sample : 01
                                        
    Remarks: 
       -In this dataset we need to crop images.
    """
    
    def __init__(self, imgdir):
        
        super(RawFooT, self).__init__(imgdir)
        
        self.labels = []                
        
        for img in self.images:
            for i in range(4):
                self.labels.append(self.get_label(img))
        self.labels = np.asarray(self.labels)
        self.label = 'RawFooT'
            
    
    ## Returns the class of the img
    def get_class(self, img):
        """Returns the class of the given img."""
        
        head, tail = os.path.split(img)
        head = head.split('\\')
        return head[-1]
    
    

## Implementation of the PapSmear dataset
class PapSmear(ImageDataset):
    """Implementation of the PapSmear dataset .
                
    Images info:
            Is composed of 2 classes, with 204 samples per class

            Extension  : .BMP
            Resolution : 70x69 to 392x262
            shape      : (70,69,3) RGB
                
            An example of img name :  abnormal\149143370-149143378-001.BMP
                                        class : anormal    
    """

    def __init__(self, imgdir):
        
        super(PapSmear, self).__init__(imgdir)        
        self.label = 'PapSmear'
    
    
    ## Returns the class of the img
    def get_class(self, img):
        """Returns the class of the given img."""
        
        head, tail = os.path.split(img)
        head = head.split('\\')

        return head[-1]
    

     
## Implementation of the PlantLeaves dataset
class PlantLeaves(ImageDataset):
    """Implementation of the PlantLeaves dataset.
                
    Images info:
            Is composed of 20 classes. The images were acquired using a planar 
            scanner and have a dimension of 128px x 128px.

            Extension  : .png
            Resolution : 128 x 128
            shape      : (128,128,3) RGB
                
            An example of img name :  c01_001_a_w01.png
                                        class : c01
                                        sample: 001.01
    """
    
    def __init__(self, imgdir):
        
        super(PlantLeaves, self).__init__(imgdir)        
        self.label = 'PlantLeaves'
    
    
    ## Returns the class of the img
    def get_class(self, img):
        """Returns the class of the given img."""
        
        head, tail = os.path.split(img)
        return tail[:3]
    
    

## Implementation of the Kather dataset class
class Kather(ImageDataset):
    """Implementation of the Kather dataset of images.
    
    Images info:
            
            Is composed by 8 classes with 626 samples per class.
            
            Extension   : .tif
            Resolution  : 150x150
            shape       : (150, 150, 3) RGB 
            
            
    Image Example : 02_STROMA\10A0_CRC-Prim-HE-07_014.tif_Row_301_Col_2401.tif
                    class : 02_STROMA
    """

    def __init__(self, imgdir):
        
        super(Kather, self).__init__(imgdir)
        self.label = 'Kather'
            
      
    ## Returns the class of the img
    def get_class(self, img):
        """Returns the class of the given img."""
        
        head, tail = os.path.split(img)
        head = head.split('\\')
        return head[-1]