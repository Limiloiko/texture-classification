# -*- coding: utf-8 -*-
"""thread_class.py 

%%%%%%%%%%%%%%%%%%%%%%%

Texture Classification Thread_class

%%%%%%%%%%%%%%%%%%%%%%%

    Author: David Lima          ---> davidlimastor@hotmail.com
            Antonio Fernández   ---> antfdez@uvigo.es  
            
        
    Last change: 05/07/2017 Vigo
    
    Language: Python 2.7
"""

import threading


class myThread(threading.Thread):
    """Thread class with a stop() method. The thread itself has to check
    regularly for the stopped() condition."""

    def __init__(self, target):
        super(myThread, self).__init__(target=target)
        self._stop_event = threading.Event()

    def stop(self):
        self._stop_event.set()

    def stopped(self):
        return self._stop_event.is_set()