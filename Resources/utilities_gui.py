# -*- coding: utf-8 -*-
"""utilities_gui.py 

%%%%%%%%%%%%%%%%%%%%%%%

Texture Classification Tool auxiliary functions

%%%%%%%%%%%%%%%%%%%%%%%

    Author: David Lima          ---> davidlimastor@hotmail.com
            Antonio Fernández   ---> antfdez@uvigo.es  
            
        
    Last change: 22/04/2017 Vigo
    
    Language: Python 2.7
    
    Requirements: skimage, pickle
    
"""


__all__ = []
__version__ = '1.0'
__author__ = 'davidlimastor@hotmail.com, antfdez@uvigo.es'


## Module imports
# Standard libraries
import os
import numpy as np
import pickle
from time import ctime

from skimage import io

import sklearn_wrappers as cw

### Path functions
## Method that helps saving an object in a .txt file using pickle
def save_to_disk(var, PathName):
    """Method that helps saving an object in a .txt file using pickle
        
        
    Args:
        var (object):Object that will be saved in the .txt file.
        PathName (str):Path where the object will be saved.
    """
    
    try:
        with open(PathName, 'wb') as results_file:
            pickle.dump(var, results_file)
            
    except:
        print 'Exception on save_to_disk function'
        raise
    
    
def check_if_exists(folder):
    """ Check if the folder exists, create it if not."""

    if not os.path.exists(folder):
        os.makedirs(folder)
        
      
## Function that helps loading an object saved in a .txt file using pickle
def load_from_disk(PathName):
    """Function that helps loading an object saved in a .txt file using pickle.
    
    Args:
        PathName (str): Path where is the .txt file.
        
    Returns:
        The object.
    """
    
    try:
        with open(PathName, 'rb') as results_file:
            var = pickle.load(results_file)
        return var
    
    except:
        print 'Exception on load_from_disk'
        raise
        
        
## Returns the path of the .txt with the calculated features with the given
## descriptor of the given dataset
def features_fname(dataset, descriptor, results_folder):
    """Returns the path of the .txt file with the calculated features with the
    given descriptor of the given dataset.
        
    Args:
        dataset     : Name of the dataset of the img
        descriptor  : Name of the descriptor 
        results_folfer: Path of the folder where is the file
        
    Returns(str) :
        The complete path of the .txt file with the dataset features
        calculated with the given descriptor.
    """
    
    try:
        des_folder = os.path.join(results_folder, descriptor.name)
        check_if_exists(des_folder)
        
        return os.path.join(des_folder, 
                            '%s--%s.txt'%(dataset.name, descriptor.__str__()))
        
    except:
        print 'Exception in utilities.features_fname'
        raise
        
              

def classifier_fname(dataset, descriptor, classifier, classifiers_folder):
    """Returns the name of the .txt with classifier."""
    
    return os.path.join(classifiers_folder,
                        '%s--%s--%s.txt' % (dataset.name, 
                                            descriptor.__str__(), 
                                            classifier.label))


def PCA_fname(dataset, descriptor, classifier, PCA_folder):
    """Returns the name of the .txt with classifier in PCA folder."""
    
    return os.path.join(PCA_folder, 
                        '%s--%s--%s.txt' % (dataset.name,
                                            descriptor.__str__(), 
                                            classifier.label))


def accuracies_fname(dataset,descriptor,validator,classifier,results_folder):
    """Returns the name of the .txt in the results_folder with classifier 
    and validator."""
    
    return os.path.join(results_folder,
                        descriptor.name,
                        '%s--%s--%s--%s.txt' % (dataset.name, 
                                                descriptor.__str__(), 
                                                validator.__str__(), 
                                                classifier.__str__()))
    

#### Processing functions ####
## Compute the features of the dataset with the given descriptor
def compute_features(dataset, descriptor, print_info='True'):
    """
        Compute the features of the dataset with the given descriptor.
        
    Args:
        dataset     : The dataset of images
        descriptor  : The descriptor that calculates the features
        print_info (bool)  : Default=True To print img path while computed 
    
    Returns:
        An np.array with the features of the dataset.
    """

    try:    
        if (dataset.name == 'Kylberg-Sintorn'):
            # Need to crop images.
            from skimage.util import view_as_blocks
            
            X = np.zeros(shape=(len(dataset.labels), descriptor.dim), 
                         dtype='float')
            index = 0
            for i, img_path in enumerate(dataset.images):
                
                img = io.imread(img_path)
                
                if img.shape[2] > 3:
                    img2 = np.zeros(shape=(img.shape[0], img.shape[1], 3), 
                                    dtype='uint8')
                    
                    img2[:,:,0] = img[:,:,0]
                    img2[:,:,1] = img[:,:,1]
                    img2[:,:,2] = img[:,:,2]
                    img = img2
                    
                # See kylberg resolution
                img_blocks = view_as_blocks(img, block_shape=(576, 864, 3))
                    
                for row in range(6):
                    for col in range(6):
                        if print_info:
                            
                            print 'block row:%s col:%s of %s '%(row,col,
                                                                img_path)
                        
                        X[index] = descriptor(img_blocks[row,col,0])
                        index = index + 1
                
        
        elif (dataset.name == 'RawFooT'):
            # Needs to crop images 
            from skimage.util import view_as_blocks
            
            X = np.zeros(shape=(len(dataset.images)*4, descriptor.dim), 
                         dtype='float')
            index = 0
            
            for i, img_path in enumerate(dataset.images):
                
                img = io.imread(img_path)
        
                img_blocks = view_as_blocks(img, block_shape=(400, 400, 3))
                
                for row in range(2):        
                    for col in range(2):            
                        if print_info:
                            
                            print 'block row:%s col:%s of %s '%(row,col,
                                                                 img_path)
                            
                        X[index] = descriptor(img_blocks[row,col,0])
                        index = index + 1
        
        
        else:
            
            X = np.zeros(shape=(len(dataset.images), descriptor.dim), 
                         dtype='float64')
            
            for i, img_path in enumerate(dataset.images):
            
                if print_info:
                    print img_path
                    
                img = io.imread(img_path)
                X[i] = descriptor(img)
                
        return X
    
    except:
        print 'Exception in utilities.compute_features'
        raise


def compute_features_GUI(parent, dataset, descriptor, print_info='True'):
    """
        Compute the features of the dataset with the given descriptor.
        
    Args:
        dataset     : The dataset of images
        descriptor  : The descriptor that calculates the features
        print_info (bool)  : Default=True To print img path while computed 
    
    Returns:
        An np.array with the features of the dataset.
    """

    try:    
        if (dataset.name == 'Kylberg-Sintorn'):
            # Need to crop images.
            from skimage.util import view_as_blocks
            
            X = np.zeros(shape=(len(dataset.labels), descriptor.dim), 
                         dtype='float')
            index = 0
            for i, img_path in enumerate(dataset.images):
                
                img = io.imread(img_path)
                
                if img.shape[2] > 3:
                    img2 = np.zeros(shape=(img.shape[0], img.shape[1], 3), 
                                    dtype='uint8')
                    
                    img2[:,:,0] = img[:,:,0]
                    img2[:,:,1] = img[:,:,1]
                    img2[:,:,2] = img[:,:,2]
                    img = img2
                    
                # See kylberg resolution
                img_blocks = view_as_blocks(img, block_shape=(576, 864, 3))
                    
                for row in range(6):
                    for col in range(6):
                        if print_info:
                            
                            line = 'block row:%s col:%s of %s '%(row,col,
                                                                 img_path)
                            save_log(parent, line)
                            
                            percentaje = 1.*(i*36)/len(dataset.labels)*100
                            parent.gauge.SetValue(percentaje)
                        
                        X[index] = descriptor(img_blocks[row,col,0])
                        index = index + 1
                
        
        elif (dataset.name == 'RawFooT'):
            # Needs to crop images 
            from skimage.util import view_as_blocks
            
            X = np.zeros(shape=(len(dataset.images)*4, descriptor.dim), 
                         dtype='float')
            index = 0
            
            for i, img_path in enumerate(dataset.images):
                
                img = io.imread(img_path)
        
                img_blocks = view_as_blocks(img, block_shape=(400, 400, 3))
                
                for row in range(2):        
                    for col in range(2):            
                        if print_info:
                            
                            line = 'block row:%s col:%s of %s '%(row,col,
                                                                 img_path)
                            save_log(parent, line)
                            percentaje = 1.*(i*4)/len(dataset.labels)*100
                            parent.gauge.SetValue(percentaje)
                            
                        X[index] = descriptor(img_blocks[row,col,0])
                        index = index + 1
        
        
        else:
            
            X = np.zeros(shape=(len(dataset.images), descriptor.dim), 
                         dtype='float64')
            
            for i, img_path in enumerate(dataset.images):
            
                if print_info:
                    save_log(parent, img_path)
                    parent.gauge.SetValue(1.*i/len(dataset.labels)*100)
                    
                img = io.imread(img_path)
                X[i] = descriptor(img)
                
        return X
    
    except:
        print 'Exception in utilities.compute_features'
        raise
        
        

def compute_or_read_accuracies_GUI(parent, X, y, filename, dataset, descriptor,
                                   validator,classifier, results_folder):
    
    """Compute the accuracies with the given classificator and dataset."""
    
    if classifier.best:
        classifier, reduce_dim = compute_or_read_classifier(parent, X, y, 
                                                               dataset, 
                                                               descriptor, 
                                                               classifier)
        
        filename = accuracies_fname(dataset, descriptor, validator, 
                                    classifier, results_folder)
                
    if not os.path.isfile(filename):
        
        line = '\tClassifier: ' + classifier.__str__() + '\n'
        save_log(parent, line)    
        parent.bar_label2.SetLabel('Process: Classifying ...')
        accs = np.zeros(validator.n_splits, dtype='float')
        for i, (train_index, test_index) in enumerate(validator.split(X, y)):
            
            try:
                if reduce_dim != None:
                    X = reduce_dim.fit_transform(X)
            except:
                pass
            
            classifier.fit(X[train_index], y[train_index])
            accs[i] = classifier.score(X[test_index], y[test_index])
            
        save_to_disk(accs, filename)
        
    else:
        line = '\tClassifier: ' + classifier.__str__() + '\n'
        save_log(parent, line)
        accs = load_from_disk(filename)
        
    return accs                  
         

def compute_or_read_accuracies(X, y, filename, dataset, descriptor,
                                   validator,classifier, results_folder,log):
    
    """Compute the accuracies with the given classificator and dataset."""
    
    if classifier.best:
        classifier, reduce_dim = compute_or_read_classifier(X, y, dataset, 
                                                            descriptor, 
                                                            classifier,log)
        
        filename = accuracies_fname(dataset, descriptor, validator, 
                                    classifier, results_folder)
                
    if not os.path.isfile(filename):
        
        line = '\tClassifier: ' + classifier.__str__() + '\n'
        log.write(line)    
        print line
        
        accs = np.zeros(validator.n_splits, dtype='float')
        for i, (train_index, test_index) in enumerate(validator.split(X, y)):
            
            try:
                if reduce_dim != None:
                    X = reduce_dim.fit_transform(X)
            except:
                pass
            
            classifier.fit(X[train_index], y[train_index])
            accs[i] = classifier.score(X[test_index], y[test_index])
            
        save_to_disk(accs, filename)
        
    else:
        line = '\tClassifier: ' + classifier.__str__() + '\n'
        log.write(line)
        print line
        
        accs = load_from_disk(filename)
        
    return accs      
             

def compute_or_read_classifier(parent, X, y, 
                                  dataset, descriptor, classifier):
    
    """Checks if its saved a classificator for the currently descriptor 
    and dataset."""
    
    classifiers_folder = os.path.join(os.getcwd(), 'Resources', 'Classifiers')
    check_if_exists(classifiers_folder)
    
    PCA_folder = os.path.join(os.getcwd(), 'Resources', 'PCA')
    check_if_exists(PCA_folder)
    
    c_fname = classifier_fname(dataset, descriptor, classifier, 
                               classifiers_folder)
    
    r_fname = PCA_fname(dataset, descriptor, classifier, PCA_folder)
    
    if not os.path.isfile(c_fname):
        parent.bar_label2.SetLabel('Process: Computing best classifier ...')
        # Needs to begin grid search
        
        from sklearn.model_selection import GridSearchCV
        from sklearn.model_selection import StratifiedShuffleSplit
        
        line = 'Calculating best params...'
        save_log(parent, line)
        
        if isinstance(classifier, cw.KNN):
            from sklearn.neighbors import KNeighborsClassifier
        
            # Params range
            n_range = [1,3,5]
            
            param_grid = dict(n_neighbors=n_range)
            
            # Cross Validation
            cv = StratifiedShuffleSplit(n_splits=1, test_size=0.5, 
                                        random_state=0)
            
            grid = GridSearchCV(KNeighborsClassifier(), param_grid=param_grid, 
                                cv=cv)
            grid.fit(X, y)
            
            s_1 = grid.best_params_
            s_2 = grid.best_score_
            
            line = "Best parameters are %s with a score of %0.2f"%(s_1, s_2)
            
            save_log (parent, line)
        
            classifier = cw.KNN(n_neighbors=grid.best_params_['n_neighbors'])
            
            
        elif isinstance(classifier, cw.SVM):
            from sklearn.svm import SVC
            
            # Params ranges
            C_range = [2**i for i in [-5,-3,-1,1,3,5,7,9,11,13,15]]
            gamma_range = [2**j for j in [-5,-3,-1,1,3,5,7,9,11,13,15]]
    
            param_grid = dict(gamma=gamma_range, C=C_range)
            
            # Cross Validation
            cv = StratifiedShuffleSplit(n_splits=1, test_size=0.5, 
                                        random_state=0)
            
            grid = GridSearchCV(SVC(), param_grid=param_grid, cv=cv)
            grid.fit(X, y)
            
            s_1 = grid.best_params_
            s_2 = grid.best_score_
            
            line = "Best parameters are %s with a score of %0.2f"%(s_1,s_2)
            
            save_log (parent, line)
            
            classifier = cw.SVM(C=grid.best_params_['C'],
                                gamma=grid.best_params_['gamma'])
            
            
        elif isinstance(classifier, cw.DTC):
            
            from sklearn.tree import DecisionTreeClassifier
            from sklearn.pipeline import Pipeline
            from sklearn.decomposition import PCA
            
            pipe = Pipeline(
                [('reduce_dim', PCA()), 
                 ('clf', DecisionTreeClassifier())])
            
            depth_range = [9,11,13,15,17,19,21,23,25]
            min_samples_range = [1,2,3,4,5,6,7]
            samples_split_range = [2,3,4,5]
    
            N_FEATURES_OPTIONS = range(10,25,3)

            param_grid = {
                    'reduce_dim__n_components': N_FEATURES_OPTIONS,
                    'clf__max_depth': depth_range,
                    'clf__min_samples_leaf': min_samples_range,
                    'clf__min_samples_split': samples_split_range,
                        }
    
            cv = StratifiedShuffleSplit(n_splits=3, test_size=0.5,
                                        random_state=0)
            
            grid = GridSearchCV(pipe, param_grid=param_grid, cv=cv)
            grid.fit(X, y)
                
            s_1 = grid.best_params_
            s_2 = grid.best_score_
    
            line = "Best parameters are %s with a score of %0.2f"%(s_1,s_2)
            
            save_log(parent, line)
            
            classifier = cw.DTC(grid.best_params_['clf__max_depth'],
                                grid.best_params_['clf__min_samples_leaf'],
                                grid.best_params_['clf__min_samples_split'],)
            
            reduce_dim = PCA(
                    n_components=grid.best_params_['reduce_dim__n_components'])
            
            save_to_disk(classifier, c_fname)
            save_to_disk(reduce_dim, r_fname)
            
            return classifier, reduce_dim
            
            
        save_to_disk(classifier, c_fname)
        return classifier, None
    
    
    else:
        
         if os.path.exists(r_fname):
            return load_from_disk(c_fname), load_from_disk(r_fname)

         else:
            return load_from_disk(c_fname), None


## Write in the listbox log of the gui a text
def save_log(parent, text, sep=False):
    """Function for write in the in the main_frame ListBox_Log .
        
    Args:
        parent (object): Parent window where the ListBox is placed.
        text (str): Text for show in the log.
        sep (bool): If wants to add "####" line.
    """
    
    time = ctime()[11:-5] + '   '
    index = parent.listBox_Log.GetCount()
    
    parent.listBox_Log.InsertItems(items= [time+text], pos=index)
    if sep:
        parent.listBox_Log.InsertItems(items=['########################'], 
                                       pos=index+1)
        
        parent.listBox_Log.SetSelection(index+1)
        return
        
    parent.listBox_Log.SetSelection(index)
    
   
def my_window(img, pixel, radius, color=True):
    # !!!
    P = 2*radius + 1
    return P


## Returns an array with the values for each window in the image(img) 
def window(img, pixel, radius, color=True):
    """Returns an array with the values for each window in the image(img).
        
    Args: 
        img (np.array): Array with all values.
        pixel (int): Pixel of the 3x3.
        color (bool): Choose return RGB (True), or a Grey level.
        radius(int): Select the scale .
            
    Returns:
        Returns an array with the values of RGB or grey pixels in the 
        selected region of the image.
    """
    
    try:
        
        if radius == 1:
        
            if pixel == 0:
                return img[ 1:-1, :-2 , :] if color else img[1:-1,  :-2]
            elif pixel == 1:
                return img[ 2:  , :-2 , :] if color else img[2:  ,  :-2]
            elif pixel == 2:
                return img[2 :  , 1:-1, :] if color else img[2:  , 1:-1]
            elif pixel == 3:
                return img[2 :  , 2:  , :] if color else img[2:  , 2:  ]
            elif pixel == 4:
                return img[1 :-1, 2:  , :] if color else img[1:-1, 2:  ]
            elif pixel == 5:
                return img[  :-2, 2:  , :] if color else img[:-2 , 2:  ]
            elif pixel == 6:
                return img[  :-2, 1:-1, :] if color else img[:-2 , 1:-1]
            elif pixel == 7:
                return img[  :-2,  :-2, :] if color else img[:-2 ,  :-2]
            
        elif radius == 2:
            
            if pixel == 0:
                return img[2:-2, :-4, :] if color else img[2:-2, :-4]
            elif pixel == 1:
                return img[3:-1, :-4, :] if color else img[3:-1, :-4]
            elif pixel == 2:
                return img[4:  , :-4, :] if color else img[4:  , :-4]
            elif pixel == 3:
                return img[4:  ,1:-3, :] if color else img[4:  ,1:-3]
            elif pixel == 4:
                return img[4:  ,2:-2, :] if color else img[4:  ,2:-2]
            elif pixel == 5:
                return img[4:  ,3:-1, :] if color else img[4:  ,3:-1]
            elif pixel == 6:
                return img[4:  ,4:  , :] if color else img[4:  ,4:  ]
            elif pixel == 7:
                return img[3:-1,4:  , :] if color else img[3:-1,4:  ]
            elif pixel == 8:
                return img[2:-2,4:  , :] if color else img[2:-2,4:  ]
            elif pixel == 9:
                return img[1:-3,4:  , :] if color else img[1:-3,4:  ]
            elif pixel == 10:
                return img[ :-4,4:  , :] if color else img[ :-4,4:  ]
            elif pixel == 11:
                return img[ :-4,3:-1, :] if color else img[ :-4,3:-1]
            elif pixel == 12:
                return img[ :-4,2:-2, :] if color else img[ :-4,2:-2]
            elif pixel == 13:
                return img[ :-4,1:-3, :] if color else img[ :-4,1:-3]
            elif pixel == 14:
                return img[ :-4, :-4, :] if color else img[ :-4, :-4]
            elif pixel == 15:
                return img[1:-3, :-4, :] if color else img[1:-3, :-4]
            
        elif radius == 3:
            
            if pixel == 0:
                return img[3:-3, :-6, :] if color else img[3:-3, :-6]
            elif pixel == 1:
                return img[4:-2, :-6, :] if color else img[4:-2, :-6]
            elif pixel == 2:
                return img[5:-1, :-6, :] if color else img[5:-1, :-6]
            elif pixel == 3:
                return img[6:  , :-6, :] if color else img[6:  , :-6]
            elif pixel == 4:
                return img[6:  ,1:-5, :] if color else img[6:  ,1:-5]
            elif pixel == 5:
                return img[6:  ,2:-4, :] if color else img[6:  ,2:-4]
            elif pixel == 6:
                return img[6:  ,3:-3, :] if color else img[6:  ,3:-3]
            elif pixel == 7:
                return img[6:  ,4:-2, :] if color else img[6:  ,4:-2]
            elif pixel == 8:
                return img[6:  ,5:-1, :] if color else img[6:  ,5:-1]
            elif pixel == 9:
                return img[6:  ,6:  , :] if color else img[6:  ,6:  ]
            elif pixel == 10:
                return img[5:-1,6:  , :] if color else img[5:-1,6:  ]
            elif pixel == 11:
                return img[4:-2,6:  , :] if color else img[4:-2,6:  ]
            elif pixel == 12:
                return img[3:-3,6:  , :] if color else img[3:-3,6:  ]
            elif pixel == 13:
                return img[2:-4,6:  , :] if color else img[2:-4,6:  ]
            elif pixel == 14:
                return img[1:-5,6:  , :] if color else img[1:-5,6:  ]
            elif pixel == 15:
                return img[ :-6,6:  , :] if color else img[ :-6,6:  ]
            elif pixel == 16:
                return img[ :-6,5:-1, :] if color else img[ :-6,5:-1]
            elif pixel == 17:
                return img[ :-6,4:-2, :] if color else img[ :-6,4:-2]
            elif pixel == 18:
                return img[ :-6,3:-3, :] if color else img[ :-6,3:-3]
            elif pixel == 19:
                return img[ :-6,2:-4, :] if color else img[ :-6,2:-4]
            elif pixel == 20:
                return img[ :-6,1:-5, :] if color else img[ :-6,1:-5]
            elif pixel == 21:
                return img[ :-6, :-6, :] if color else img[ :-6, :-6]
            elif pixel == 22:
                return img[1:-5, :-6, :] if color else img[1:-5, :-6]
            elif pixel == 23:
                return img[2:-4, :-6, :] if color else img[2:-4, :-6]
            
        else:
            raise
            
    except:
        print 'Exception in window function'
        raise