# -*- coding: utf-8 -*-
"""Tool_Source.py 

%%%%%%%%%%%%%%%%%%%%%%%

Texture Classification Main Frame

%%%%%%%%%%%%%%%%%%%%%%%

    Author: David Lima          ---> davidlimastor@hotmail.com
            Antonio Fernández   ---> antfdez@uvigo.es  
            
        
    Last change: 24/07/2017 Vigo
    
    Language: Python 2.7
    
"""
import os

# Add Resources folder with auxiliary scripts
import sys 
sys.path.insert(0, os.path.join(os.getcwd(), 'Resources'))


import wx
from time import ctime

import gui_source as gui
import utilities as ut
import image_datasets as imdata
import descriptors_class as dw
import sklearn_wrappers as cw
import EnhancedStatusBar
import thread_class



class APP(gui.Main_Frame):
    """ Main window of the application."""
    
    def __init__(self):
        """Constructor of the main window """
        
        # Import from the class created by the wxFormBuilder
        super(APP, self).__init__(None)
        
        # Set icon
        icon_path = r'Resources\Icon\ei - icono 16x16.ico'
        icon = wx.Icon(os.path.join(os.getcwd(),icon_path))
        self.SetIcon(icon)
        
        # Thread process that will execute the experiments
        self.thread_process = None
        
        # Folders
        self.data_folder = '' # Will be defined on GUI
        
        #results_path = '_Results'
        #ut.check_if_exists(results_path)
        
        #self.results_folder = os.path.join(os.getcwd(), results_path)
        self.results_folder = '' # To be defined
        
        self.data_checkboxes = [self.checkBox_Kather, self.checkBox_KTHTIPS2b, 
                                self.checkBox_KylbergSintorn, 
                                self.checkBox_MondialMarmi, 
                                self.checkBox_Outex_13, self.checkBox_PapSmear,
                                self.checkBox_PlantLeaves, 
                                self.checkBox_RawFooT, 
                                self.checkBox_STex]
        
        # Echanced status bar
        self.statusbar = EnhancedStatusBar.EnhancedStatusBar(self, -1)
        self.statusbar.SetSize((-1, 20))
        self.statusbar.SetFieldsCount(3)
        self.SetStatusBar(self.statusbar)        

        label_info = 'Descriptor: , Dataset: '
        self.bar_label1 = wx.StaticText(parent=self.statusbar,id=-1, 
                                        label=label_info,
                                        style=wx.ALIGN_LEFT)
        
        self.gauge = wx.Gauge(parent=self.statusbar, id=-1, range=99, 
                              pos=wx.DefaultPosition, 
                              size=(350,20))
        
        self.bar_label2 = wx.StaticText(parent=self.statusbar,id=-1, 
                                        label='Process: Stopped')
        
        statusbarchildren = self.statusbar.GetChildren()
        
        for widget in statusbarchildren:
            
            self.statusbar.AddWidget(widget, 
                    horizontalalignment=EnhancedStatusBar.ESB_ALIGN_LEFT)
        
        
    def combo_descriptorOnCombobox(self, event):
        """ A handle to change selection in descriptor combobox. Enables or 
        disables widgets with the descriptor specific parameters.
        """
        
        # Get descriptor name
        sel = self.combo_descriptor.GetValue()
        
        if sel in ['Order Lex','Order BitMix']:
            # Uses Bands priority
            self.combo_bands.Enable(True)
            self.panel_references.Enable(False)
            
        elif sel in ['PreOrder Ref']:
            # Uses a reference
            self.panel_references.Enable(True)
            self.combo_bands.Enable(False)
            
        else:
            self.panel_references.Enable(False)
            self.combo_bands.Enable(False)
        
        

    def button_AddOnButtonClick(self, event):
        """Add the descriptor configured to the list of descriptors.""" 
        
        des = self.combo_descriptor.GetValue()
        
        # Valid descriptor in comboBox
        if des != 'Descriptor':
            
            text = 'Descriptor: %s, ' % des
            
            if des in ['Order Lex','Order BitMix']:
                # If uses bands
                band = self.combo_bands.GetValue()
                text += 'bands prio: %s, '  % band
                
            elif des in ['PreOrder Ref']:
                # If uses a reference
                ref1 = self.textCtrl_ref1.GetValue()
                ref2 = self.textCtrl_ref2.GetValue()
                ref3 = self.textCtrl_ref3.GetValue()
                
                try:
                    # Try to convert the contents str into int
                    ref = [int(ref1), int(ref2),int(ref3)]
                    text += 'Reference: %s, '  % str(ref)
                except:
                    wx.MessageBox('Please, enter numbers in references', 
                                  'Error', wx.OK)
                    
                    self.textCtrl_ref1.SetValue('0')
                    self.textCtrl_ref2.SetValue('0')
                    self.textCtrl_ref3.SetValue('0')
                    return
            # All uses scales
            scale = self.combo_Scale.GetValue()
            text += 'Scale: %s'  % scale
            
            # Add the descriptor to the descriptors listBox 
            self.listBox_Descriptors.InsertItems(items=[text], pos=0)
            self.textCtrl_ref1.SetValue('0')
            self.textCtrl_ref2.SetValue('0')
            self.textCtrl_ref3.SetValue('0')



    def button_DeleteOnButtonClick(self, event):
        """Button to delete descriptors in the comboBox"""
        
        sel = self.listBox_Descriptors.GetSelection()
        
        if sel != wx.NOT_FOUND:
            self.listBox_Descriptors.Delete(sel)



    def dirPickerOnDirChanged(self, event):
        """Change the folder with the imgs, enables the datasets checkbox if 
        there is a folder with his name."""
        
        self.data_folder = self.dirPicker.GetPath()

        results_path = '_Results'
        self.results_folder = os.path.join(self.data_folder, results_path)
        ut.check_if_exists(self.results_folder)

        
        text = 'Folder with datasets: %s ' % self.data_folder
        ut.save_log(self, text)
        
        root, dirs, files = os.walk(self.data_folder).next()
        
        datasets = ['Kather','KTH-TIPS2-b','Kylberg-Sintorn','MondialMarmi20',
                    'Outex-13','PapSmear','PlantLeaves','RawFooT','STex']
        
        for i in self.data_checkboxes:
            i.Enable(False)
        
        
        for n, dataset in enumerate(datasets):
            if dataset in dirs:
                self.data_checkboxes[n].Enable(True)
            
        text = 'Datasets updated'
        ut.save_log(self, text, sep=True)
        
        
        
    def combo_validatorOnCombobox(self, event):
        """Enables or disables the textCtrl with the split"""
        
        # Get validator name
        sel = self.combo_validator.GetValue()
        
        if sel in ['Stratified Sampling Train']:
            # Uses KNN_N 
            self.textCtrl_SSS.Enable(True)

        else:
            self.textCtrl_SSS.Enable(False)
        
        
        
    def button_AddValidatorOnButtonClick(self, event):
        """Add validator information to the validators listBox."""
        
        val = self.combo_validator.GetValue()
        
        if val != 'Validator':
            split = self.textCtrl_SSS.GetValue()
            
            try:
                x = float(split)
                if x >= 1.0 or x <= 0.0 :
                    wx.MessageBox('Splits limits are [0.0,1.0]','Error', wx.OK)
                    self.textCtrl_SSS.SetValue('')
                    return
                
                text = val + ' Split= %s' % split
                
            except:
                wx.MessageBox('Incorrect value for Split SSS','Error', wx.OK)
                self.textCtrl_SSS.SetValue('')
                return            
            
            
            self.listBox_Validators.InsertItems(items=[text], pos=0)
            self.textCtrl_SSS.SetValue('')
            
            
    def button_DeleteValidatorOnButtonClick(self, event):
        """Delete the selected item in listBox validators."""
        
        sel = self.listBox_Validators.GetSelection()
        
        if sel != wx.NOT_FOUND:
            self.listBox_Validators.Delete(sel)

    

    def combo_classifierOnCombobox(self, event):
        """Enables or disables text controls depending on waht is selected."""
        
        self.check_Best.Set3StateValue(False)
        # Get classifier name
        sel = self.combo_classifier.GetValue()
        
        if sel in ['KNN']:
            # Uses KNN_N 
            self.textCtrl_N.Enable(True)
            self.textCtrl_C.Enable(False)
            self.textCtrl_G.Enable(False)
            self.textCtrl_Depth.Enable(False)
            self.textCtrl_Leaf.Enable(False)
            
        elif sel in ['SVM']:
            # Uses SVM_C and SVM_G 
            self.textCtrl_N.Enable(False)
            self.textCtrl_C.Enable(True)
            self.textCtrl_G.Enable(True)
            self.textCtrl_Depth.Enable(False)
            self.textCtrl_Leaf.Enable(False)
            
        elif sel in ['DTC']:
            # Uses DTC_Depth and DTC_Leaf
            self.textCtrl_N.Enable(False)
            self.textCtrl_C.Enable(False)
            self.textCtrl_G.Enable(False)
            self.textCtrl_Depth.Enable(True)
            self.textCtrl_Leaf.Enable(True)
            
        else:
            self.textCtrl_N.Enable(False)
            self.textCtrl_C.Enable(False)
            self.textCtrl_G.Enable(False)
            self.textCtrl_Depth.Enable(False)
            self.textCtrl_Leaf.Enable(False)
            
    
    def check_BestOnCheckBox(self, event):
        """If best parameters will be calculated for the selected classisifer.
        """
        
        state = self.check_Best.IsChecked()
        
        if state:
            self.textCtrl_N.Enable(False)
            self.textCtrl_C.Enable(False)
            self.textCtrl_G.Enable(False)
            self.textCtrl_Depth.Enable(False)
            self.textCtrl_Leaf.Enable(False)
        
        else:
            # Get classifier name
            sel = self.combo_classifier.GetValue()
            
            if sel in ['KNN']:
                # Uses KNN_N 
                self.textCtrl_N.Enable(True)
                self.textCtrl_C.Enable(False)
                self.textCtrl_G.Enable(False)
                self.textCtrl_Depth.Enable(False)
                self.textCtrl_Leaf.Enable(False)
                
            elif sel in ['SVM']:
                # Uses SVM_C and SVM_G 
                self.textCtrl_N.Enable(False)
                self.textCtrl_C.Enable(True)
                self.textCtrl_G.Enable(True)
                self.textCtrl_Depth.Enable(False)
                self.textCtrl_Leaf.Enable(False)
                
            elif sel in ['DTC']:
                # Uses DTC_Depth and DTC_Leaf
                self.textCtrl_N.Enable(False)
                self.textCtrl_C.Enable(False)
                self.textCtrl_G.Enable(False)
                self.textCtrl_Depth.Enable(True)
                self.textCtrl_Leaf.Enable(True)
                
            else:
                self.textCtrl_N.Enable(False)
                self.textCtrl_C.Enable(False)
                self.textCtrl_G.Enable(False)
                self.textCtrl_Depth.Enable(False)
                self.textCtrl_Leaf.Enable(False)
            
    

            
    def button_AddClassifierOnButtonClick(self, event):
        """Add classifier information to the classifiers listBox."""
        
        cls = self.combo_classifier.GetValue()
        
        # Valid classifier in comboBox
        if cls != 'Classifier':
            
            text =  cls
            
            if cls in ['KNN']:
                
                if self.check_Best.IsChecked():
                    text += ' with best params'
                    
                else:
                    # KNN_N
                    N = self.textCtrl_N.GetValue()
                
                    try:
                        int(N)
                        text += ' N= %s' % N
                        
                    except:
                        error = 'Please enter integers in KNN_N paremeter'
                        wx.MessageBox(error, 'Error', wx.OK)
                        self.textCtrl_N.SetValue('')
                        return
                    
                
            elif cls in ['SVM']:
                
                if self.check_Best.IsChecked():
                    text += ' with best params'
                
                else:
                    # Uses C and gamma
                    SVM_C = self.textCtrl_C.GetValue()
                    SVM_G = self.textCtrl_G.GetValue()
                    
                    try:
                        # Try to convert the contents str into floats
                        float(SVM_C)
                        float(SVM_G)
                    
                        text += ' C= %s, G= %s'  % (SVM_C, SVM_G)
                    except:
                        error = 'Please, enter floats in SVM parameters'
                        wx.MessageBox(error,'Error', wx.OK)
                        
                        self.textCtrl_C.SetValue('')
                        self.textCtrl_G.SetValue('')
                        return
                    
                    
            elif cls in ['DTC']:
                
                if self.check_Best.IsChecked():
                    text += ' with best params'
                
                else:
                    # Uses max_Depth and min_samples_leaf
                    DTC_Depth = self.textCtrl_Depth.GetValue()
                    DTC_Leaf = self.textCtrl_Leaf.GetValue()
                    
                    text += ' depth= %s, samples_leaf= %s' % (DTC_Depth, 
                                                              DTC_Leaf)
                    
                    try:
                        # Try to convert the contents in float
                        float(DTC_Depth)
                        float(DTC_Leaf)
                        
                    except:
                        error = 'Please, enter floats in DTC parameters'
                        wx.MessageBox(error,'Error', wx.OK)
                        
                        self.textCtrl_Depth.SetValue('')
                        self.textCtrl_Leaf.SetValue('')
                        return
            
                    
            # Add the descriptor to the descriptors listBox 
            self.listBox_Classifiers.InsertItems(items=[text], pos=0)
            self.textCtrl_N.SetValue('')
            self.textCtrl_C.SetValue('')
            self.textCtrl_G.SetValue('')
            self.textCtrl_Depth.SetValue('')
            self.textCtrl_Leaf.SetValue('')


    
    def button_DeleteClassifierOnButtonClick(self, event):
        """Delete the selected item in listBox classifier.s"""
        
        
        sel = self.listBox_Classifiers.GetSelection()
        
        if sel != wx.NOT_FOUND:
            self.listBox_Classifiers.Delete(sel)



    def m_menuexitOnMenuSelection(self, event):
        """A handle to exit on menu selection. 
        
        Creates de log.txt file with the experiments information and closes the 
        GUI."""
        
        actualtime = ctime()
        
        # Log folder, check if exists
        folder = os.path.join(self.data_folder, '_Log')
        ut.check_if_exists(folder)
        
        log = os.path.join(folder, actualtime.replace(':', '-') + 'GUI.txt')
        
        log = open(log, 'w')
        
        # Write the .txt
        for pos in list(reversed(range(self.listBox_Log.GetCount()))):
            
            line = self.listBox_Log.GetString(pos)
            log.writelines(line + '\n')
        
        log.close()
        
        try:
            self.thread_process.stop()
        except:
            pass
        
        self.Destroy()
        
        
    def Start(self):
        """A handle to start the process."""
        
        line = 'Initializing process ...'
        ut.save_log(self, line)
        
        # First gets the list of datasets to use in the session
        self.datasets = []
        datasets_names = ['Kather', 'KTH-TIPS2-b', 'Kylberg-Sintorn',
                          'MondialMarmi20','Outex-13','PapSmear',
                          'PlantLeaves', 'RawFooT','STex']
        
        datasets_classes = [imdata.Kather, imdata.KTHTIPS2b,
                            imdata.KylbergSintorn,
                            imdata.MondialMarmi20,
                            imdata.Outex_13,imdata.PapSmear,imdata.PlantLeaves,
                            imdata.RawFooT,imdata.STex]
        
        for n,box in enumerate(self.data_checkboxes):
            if box.IsChecked():
                self.datasets.append(datasets_classes[n](os.path.join(
                        self.data_folder, datasets_names[n])))
                
        # Gets the list with descriptors
        self.descriptors = []
        
        descriptors_classes = {'Rank' : dw.Rank_Transform,
                        'Order Lex' : dw.Rank_Color_Order_Lexicographic,
                        'Order BitMix' : dw.Rank_Color_Order_BitMixing,
                        'PreOrder Ref' :dw.Rank_Color_PreOrder_ColorRef,
                        'Partial Order' : dw.Rank_Color_PartialOrder}
        
        for index in list(reversed(range(self.listBox_Descriptors.GetCount()))):
            line = self.listBox_Descriptors.GetString(index)
        
            # Gets the name
            search = 'Descriptor: '
            ind_f = line.find(search) + len(search)
            ind_l = line.find(',')
            
            des = line[ind_f:ind_l]  
        
        
            # Gets the scale
            search = 'Scale: '
            ind_f = line.find(search) + len(search)
            
            
            p_r = line[ind_f:]
            
            p = eval(p_r[2:p_r.find(']')+1])
            r = eval(p_r[p_r.find(',r=')+3:])
            
            
            # If Lex or BitMix gets the bands
            if des in ['Order Lex','Order BitMix']:
                search = 'bands prio: '
                ind_f = line.find(search) + len(search)
                
                bands = line[ind_f:ind_f+3]
                
                self.descriptors.append(descriptors_classes[des](bands, p, r))


            elif des in ['PreOrder Ref']:
                
                search = 'Reference: '
                ind_f = line.find(search) + len(search)
                ind_l = line[:].find(']') + 1
                reference = eval(line[ind_f:ind_l])
                
                self.descriptors.append(descriptors_classes[des](p,
                                                                 r,
                                                                 reference))
            
            else:
                
                self.descriptors.append(descriptors_classes[des](r))
            
        self.listBox_Descriptors.Clear()
            
        # gets the list with the validators
        self.validators = []
        
        validators_classes = {'Stratified Sampling Train': cw.SSS}
        
        for index in list(reversed(range(self.listBox_Validators.GetCount()))):
            line = self.listBox_Validators.GetString(index)
            
            SSS = 'Stratified Sampling Train'
            
            if line.find(SSS) != -1:
                search = 'Split= '
                split = float(line[line.find(search)+len(search):])
                
                self.validators.append(
                        validators_classes[SSS](train_size=split))
        
        self.listBox_Validators.Clear()
        
        # Gets the list with the classifiers
        self.classifiers = []
        
        classifiers_classes = {'KNN':cw.KNN, 'SVM':cw.SVM, 'DTC':cw.DTC}
        
        for index in list(reversed(range(self.listBox_Classifiers.GetCount()))):
            line = self.listBox_Classifiers.GetString(index)
            
            if line[:3] in ['KNN']:
                if not 'with best params' in line:
                    n_neighbors = int(line[line.find('N=')+3:])
                    self.classifiers.append(classifiers_classes['KNN'](
                                                    n_neighbors=n_neighbors))
                
                else:
                    self.classifiers.append(classifiers_classes['KNN'](
                                                                    best=True))
                
            elif line[:3] in ['SVM']:
                if not 'with best params' in line:
                    C = float(line[line.find('C=')+3:line.find(',')])
                    G = float(line[line.find('G=')+3:])
                    self.classifiers.append(classifiers_classes['SVM'](C=C, 
                                                                    gamma=G))
                    
                else:
                    self.classifiers.append(classifiers_classes['SVM'](
                                                                    best=True))
                    
            elif line[:3] in ['DTC']:
                if not 'with best params' in line:
                    
                    f = line.find('depth=')+7
                    l = line.find(',')
                    depth = float(line[f:l])
                    
                    f = line.find('samples_leaf=')+14
                    l = line.find(',')
                    
                    leaf = float(line[f:l])
                    
                    self.classifiers.append(classifiers_classes['DTC'](
                                                        max_depth=depth, 
                                                        min_samples_leaf=leaf))
        
                else:
                    self.classifiers.append(classifiers_classes['DTC'](
                                                                    best=True))
            
                    
        self.listBox_Classifiers.Clear()
        
        line = 'Datasets: %s' % [n.label for n in self.datasets]
        ut.save_log(self, line)
            
        line = 'Descriptors: %s' % [d.__str__() for d in self.descriptors]
        ut.save_log(self, line, sep=True)
        
        # Read images from disk and compute features (if necessary) 
        self.bar_label2.SetLabel('Process: Computing features ...')
        for dataset in self.datasets:
            
            for descriptor in self.descriptors:
                
                feats_fname = ut.features_fname(dataset, 
                                                descriptor, 
                                                self.results_folder)
                
                self.gauge.SetValue(0)
                
                if not os.path.isfile(feats_fname):
                    label_gauge ='Dataset: %s, Descriptor: %s'%(dataset.label, 
                                                          descriptor.__str__())
                    
                    self.bar_label1.SetLabel(label_gauge)
                    
                    X = ut.compute_features_GUI(self, dataset, descriptor, 
                                                print_info='True')
                    
                    ut.save_to_disk(X, feats_fname)
         

        # Read features from disk, split into train/test, 
        # classify and compute success rate
        self.bar_label2.SetLabel('Process: Initializing classification ...')
        for dataset in self.datasets:
            line = dataset.name 
            ut.save_log(self, line, sep=True)
            
            for descriptor in self.descriptors:
                line = '--------------------'
                ut.save_log(self, line)
                line =  'Descriptor: %s' % descriptor.__str__() 
                ut.save_log(self, line)
                
                feats_fname = ut.features_fname(dataset, descriptor, 
                                                self.results_folder)
                
                for validator in self.validators:
                    line = 'Validator: ' + validator.__str__()
                    ut.save_log(self, line)
                    
                    
                    for classifier in self.classifiers:
                        
                        accs_fname = ut.accuracies_fname(dataset, descriptor, 
                                                         validator,classifier,
                                                      self.results_folder)
                        
                        X = ut.load_from_disk(feats_fname)
                        
                        y = dataset.labels
                        
                        success_rates = ut.compute_or_read_accuracies_GUI(
                                                        self, X, y, accs_fname, 
                                                        dataset, descriptor,
                                                        validator, classifier, 
                                                        self.results_folder)
                        
                        line = 'Average success rate = %g %%'%(
                                                    success_rates.mean()*100,)
                        
                        ut.save_log(self, line)
                        
        self.bar_label2.SetLabel('Process: Stopped')

        
        
    def m_StartOnMenuSelection(self, event):
        """A handle to start or F5 shortkey. Initialize the process, create 
        a thread to do all related with it."""
        
        self.gauge.SetValue(0)
        self.bar_label2.SetLabel('Process: Initializing process ...')
        
        self.thread_process = thread_class.myThread(target=self.Start)

        self.thread_process.start()
    
        
    def m_StopOnMenuSelection(self, event):
        """A handle to stop provess on menu selection."""
        
        try:
            line = 'Stoping process...'
            ut.save_log(self, line)
            self.thread_process.stop()
            self.thread_process.Destroy()
            
        except:
            pass


    def m_ResetOnMenuSelection(self, event):
        """Clear all listboxes."""
        
        self.listBox_Classifiers.Clear()
        self.listBox_Descriptors.Clear()
        self.listBox_Validators.Clear()
        self.listBox_Log.Clear()
        
    
    def m_clearOnMenuSelection(self, event):
        """Clear log listbox."""
        
        self.listBox_Log.Clear()
        


if __name__ == '__main__':
        
    app = wx.App(False)
    
    # Launch the application
    Main = APP()
    Main.Show()
    app.MainLoop()
    app.Destroy()
