# Texture Classification Application


## Dependencies

* Python 2.7
* From SciPy:
    + Scipy 0.19.0
    + Numpy 1.11.3
    + scikit-image 0.12.3
    + scikit-learn 0.18.1
    + matplotlib 2.0.0
* wxPython 3.0.0.0


You can use:

`$ pip install -r requirements.txt `

to install all dependencies using pip resource.



## Remarks

- All calculations derived from the use of the application are stored on disk so that you do not have to recalculate them in the future.

- To correctly detect the datasets that you want to use in the experiments, you may select the main folder with all datasets with the SAME names as is written in the application.

- The first use of the BitMixing descriptor will generate a lut.txt file (457 MB), so the initializing process will take few minutes (5 minutes for me).



